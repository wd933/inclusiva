/*--
* Template Name: Palki
* Template URL: http://themehappy.com/html/palki
* Author: themehappy
* Author URI: http://themehappy.com
* Version: 1.0
* Description: Palki - Responsive Bootstrap 3  Page Template
--*/

(function ($) {
	'use strict';

	jQuery(document).ready(function ($) {
		
		$('.multicolor').each(function(){
			$(this).attr('data-text', $(this).html());
		});
		//*~-~-~- Preloader Js ~-~-~- */
		$(window).on("load", function () {
			$('.spinner').fadeOut();
			$('.preloader-area').delay(350).fadeOut('slow');
		});

		/*~-~-~- Sticky Menu ~-~-~- */

		$(window).on('scroll', function () {
			if ($(window).scrollTop() > 120) {
				$('.navbar-fixed-top').addClass('sticky');
				// $('.navbar-default').addClass('navbar-fixed-top');
			 // 	$('.navbar-default').addClass('sticky');
			} else {
				$('.navbar-fixed-top').removeClass('sticky');
				// $('.navbar-default').removeClass('navbar-fixed-top');
			 // 	$('.navbar-default').removeClass('sticky');
			}
		});


		$('a.smoth-scroll').on("click", function (e) {
			var anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top - 50
			}, 1000);
			e.preventDefault();
		});

		//$('body').append('<div id="scrollup"><i class="fa fa-angle-up"></i></div>');


		$(window).on("scroll", function () {
			if ($(this).scrollTop() > 250) {
				$('#scrollup').fadeIn();
			} else {
				$('#scrollup').fadeOut();
			}
		});
		$('#scrollup').on("click", function () {
			$("html, body").animate({
				scrollTop: 0
			}, 800);
			return false;
		});

		var filterList = {    
	        init: function () {        
	            $('#portfoliolist').mixItUp({
	                selectors: {
	              target: '.portfolio',
	              filter: '.filter' 
	          },
	          load: {
	              filter: '.insght'
	            }     
	            });
	        }
	    };    
	    filterList.init();


	    /* --- BOTTOM TO TOP --- */
		$('body').append('<div id="scrollup"><i class="fa fa-angle-up"></i></div>');
		$(window).on("scroll", function () {
			if ($(this).scrollTop() > 250) {
				$('#scrollup').fadeIn();
			} else {
				$('#scrollup').fadeOut();
			}
		});
		$('#scrollup').on("click", function () {
			$("html, body").animate({
				scrollTop: 0
			}, 800);
			return false;
		});
		
		$(".carousel").on("touchstart", function(event){
		    var xClick = event.originalEvent.touches[0].pageX;
		    $(this).one("touchmove", function(event){
		        var xMove = event.originalEvent.touches[0].pageX;
		        if( Math.floor(xClick - xMove) > 5 ){
		            $(this).carousel('next');
		        }
		        else if( Math.floor(xClick - xMove) < -5 ){
		            $(this).carousel('prev');
		        }
		    });
		    $(".carousel").on("touchend", function(){
		            $(this).off("touchmove");
		    });
		});


	});

})(jQuery);