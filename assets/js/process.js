$(document).on('submit', '.form', function () {
    var frm = $(this);
    var btn = frm.find(".save");
    var method = frm.attr("method");
    btn.attr("disabled", "disabled");
    $.ajax({
        url: frm.attr('action'),
        type: method,
        data: frm.serialize(),
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
    })
            .done(function (data)
            {
                console.log(data);
                frm.find('.response').html(data.alert).hide().slideDown();
                frm.find('.response').html(data.redirect);
                
                btn.removeAttr("disabled");
            })
            .fail(function (data, msg)
            {
                btn.removeAttr("disabled");
            });
    return false;
});



$(document).on('click', '.show-video-test', function (e) {
    e.preventDefault();
    //var url = $(this).attr("data-url");
    var id = $(this).attr("data-id");
    var url = baseUrl + 'show_video_testimonio';
    $.post(url,
        {id: id},
        function (response) {
            $("#tituloModal").html(response.titulo);
            $("#modalContenido").modal({ backdrop: 'static', keyboard: false });
            $("#modalContenidoBody").html(response.contenido);
            $("#modalContenido").modal("show");
    }, 'json');
});


// PARA CERRAR EL MODAL
$(document).on('click', '.m-close', function (e) {
    e.preventDefault();
    $("#modalContenido").modal("hide");
    $("#modalPassword").modal("hide");
});



$(document).on('click', '.send-wsp', function (e) {
    e.preventDefault();
    var _number = $(this).attr("data-number");

    var text = 'Estoy%20interesada(o)%20en%20agendar%20una%20consulta';
    var url = 'https://api.whatsapp.com/send?phone=51'+_number+'&text='+text;
    window.open(url , '_blank');
});