<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{$titulo_pagina}</title>
        <link rel="icon" href="{$baseUrl}public/imagen/favicon/{if isset($favicon_logo)}{$favicon_logo}{/if}" type="image/icon" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{$baseUrl}assets/plugins/bootstrap/css/bootstrap.min.css">
        <script> var base_url = "{$baseUrl}";</script>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{$baseUrl}assets/plugins/font-awesome/css/font-awesome.min.css">

        <!-- Theme Style -->
        <link href="{$baseUrl}assets/admin/theme/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <link href="{$baseUrl}assets/admin/theme/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />

        <link href="{$baseUrl}assets/admin/css/estilos.css" rel="stylesheet" type="text/css" />



        <!-- ############# DATEPICKER ############# -->
        <link rel="stylesheet" media="all" type="text/css" href="{$baseUrl}assets/plugins/datetimepicker/jquery-ui.css" />
        <link rel="stylesheet" media="all" type="text/css" href="{$baseUrl}assets/plugins/datetimepicker/jquery-ui-timepicker-addon.css" />
        <link rel="stylesheet" media="all" type="text/css" href="{$baseUrl}assets/plugins/datatable/css/dataTables.bootstrap.min.css" />

        <link rel="stylesheet" href="{$baseUrl}assets/plugins/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css">

        


    </head>
