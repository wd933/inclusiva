<!-- JQUERY -->
<script src="{$baseUrl}assets/plugins/jquery/2.1.3/jquery.min.js"></script>
<!-- SWEETALERT -->
<script src="{$baseUrl}assets/plugins/sweetalert-master/sweetalert-dev.js"></script>
<link rel="stylesheet" href="{$baseUrl}assets/plugins/sweetalert-master/sweetalert.css" type="text/css">

<script>
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '2016:2030'

        });

    });
</script>

<!-- Bootstrap 3.3.5 -->
<script src="{$baseUrl}assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="{$baseUrl}assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="{$baseUrl}assets/plugins/datatable/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('.example-table').DataTable({
            "language": {
                "sSearch":"Buscar:",
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron registros",
                "info": "P&aacutegina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros"
            }
        });
    } );

</script>

<!-- AdminLTE App -->
<script src="{$baseUrl}assets/admin/theme/js/app.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="{$baseUrl}assets/admin/theme/js/demo.js"></script>

<script src="{$baseUrl}assets/plugins/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js"></script>

<script src="{$baseUrl}assets/plugins/jqueryui/1.11.2/jquery-ui.js"></script>


<script type="text/javascript">

    $('.datepicker2').datepicker({
        dateFormat: 'dd-mm-yy',
        timeFormat: 'HH:mm',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 1,
        changeMonth: true,
        changeYear: true,
        yearRange: '2016:2020',
    });
</script>

<!-- SCRIPT -->
<script src="{$baseUrl}assets/admin/js/scripts.js"></script>
<script src="{$baseUrl}assets/admin/js/process.js?v={$time}"></script>


</body>

</html>