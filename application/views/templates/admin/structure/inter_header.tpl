<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo">
                <span class="logo-mini">I</span>
                <span class="logo-lg">INCLUSIVA</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{$emp_imagen}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{$emp_fullname} </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="{$emp_imagen}" class="img-circle" alt="User Image">
                                    <p>
                                        {$emp_fullname} - {$emp_gdescription}
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-default btn-flat m-password">Cambiar Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <span class="response"></span>
                                        <a data-url="{$baseUrl}admin/login/salir" class="btn btn-default btn-flat sclose">Salir</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{$emp_imagen}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{$emp_fullname}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <ul class="sidebar-menu">
                    <li class="header">MENU PRINCIPAL</li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/home">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>
                    
                    <li class="treeview">
                        <a href="{$baseUrl}admin/configuracion/panel">
                            <i class="fa fa-cogs"></i> <span> Configuracion</span>
                        </a>
                    </li>                    

                    <li class="treeview">
                        <a href="{$baseUrl}admin/carrusel_superior/listar">
                            <i class="fa fa-image"></i> <span> Carrusel superior pag. Hola</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/carrusel_cliente/listar">
                            <i class="fa fa-image"></i> <span> Carrusel de clientes</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/carrusel_video/listar">
                            <i class="fa fa-youtube-play"></i> <span> Carrusel de videos</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/carrusel_chamba/listar">
                            <i class="fa fa-image"></i> <span> Carrusel de Nuestra Chamba</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/carrusel_base/listar">
                            <i class="fa fa-image"></i> <span> Carrusel de La Base</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/equipo/listar">
                            <i class="fa fa-users"></i> <span> Nuestro equipo</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/cliente/listar">
                            <i class="fa fa-image"></i> <span> Logos clientes</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/socio/listar">
                            <i class="fa fa-image"></i> <span> Logos Socios</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/boletin/listar">
                            <i class="fa fa-newspaper-o"></i> <span> Boletines</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/noticia/listar">
                            <i class="fa fa-newspaper-o"></i> <span> Noticias</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/formulario_web/listar">
                            <i class="fa fa-edit"></i> <span> Formulario web</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{$baseUrl}admin/suscriptor/listar">
                            <i class="fa fa-edit"></i> <span> Suscriptores</span>
                        </a>
                    </li>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>