<div class="content-wrapper">
    <section class="content-header">
        
        <ol class="breadcrumb">
            <li class="active"><a href="{$baseUrl}admin/home"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"><a href="{$baseUrl}admin/suscriptor/listar"><i class="fa fa-list"></i> Suscriptores</a></li>
        </ol>
        
        <a href="{$baseUrl}admin/excel/exportar_suscriptor" target="_blank" class="btn btn-success">Exportar</a>
        {* <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control datepicker efi"  name="fi" value="" placeholder="Fecha de Inicio" />
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control datepicker eff"  name="fi" value="" placeholder="Fecha de Fin" />
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                </div>
            </div>
            <div class="col-md-3">
                <a class="btn btn-success export-form">Exportar</a>
            </div>
        </div> *}
    </section>

    <section class="content">
        <div class="response"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">{$titulo}</h3>
                    </div>
                    <div class="box-body">

                        <div class="direct-chat-msg table-responsive">
                                 <table class="table table-bordered table-hover example-table" style="text-align: center">
                                 <thead>
                                    <tr class="tr-lista" style="text-align: center;">
                                        <th style="width: 35px; text-align: center;">N°</th>
                                        <th style="text-align: center">Nombres</th>
                                        <th style="text-align: center">Apellidos</th>
                                        <th style="text-align: center">Correo</th>
                                        <th style="text-align: center">Lugar de trabajo</th>
                                        <th style="text-align: center">Cargo</th>
                                        <th style="text-align: center">Fecha Registro</th>
                                    </tr>
                                 </thead>
                                   
                                    {if isset($lista)}
                                    {foreach $lista as $l}
                                        <tr class="resultado">
                                            <td>{$l.numero}</td>
                                            <td>{$l.nombre}</td>
                                            <td>{$l.apellido}</td>
                                            <td>{$l.correo}</td>
                                            <td>{$l.lugar_trabajo}</td>
                                            <td>{$l.cargo}</td>
                                            <td>{$l.f_registro}</td>
                                        </tr>                                       
                                    {/foreach}
                                    {/if}
                                </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<div id="modalContenido" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="tituloModal" class="modal-title"></h4>
            </div>
            <div id="modalContenidoBody" class="modal-body">
            </div>
        </div>
    </div>
</div>
