<div class="row">                        
    <form action="{$baseUrl}admin/boletin/accion" class="form" method="POST">
    <div class="response"></div>

    <div class="col-md-12">
        <label><span class="text-red">[*]</span> Imagen:</label>
        <div class="form-group">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    {if isset($imagen)}
                        {if $imagen != ''}
                            <img src="{$baseUrl}assets/images/boletines/{$imagen}" alt="...">
                        {else}
                            <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                        {/if}
                    {else}
                        <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                    {/if}                    
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                <div>
                    <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen"></span>
                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><span class="text-red">[*]</span> Seleccionar archivo:</label>
            <div class="fileinput input-group fileinput-new" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">{if isset($archivo)}{$archivo}{/if}</span></div>
                <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Seleccionar</span><span class="fileinput-exists">Cambiar</span><input type="hidden" value="" name="..."><input type="file" name="archivo"></span>
                <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Eliminar</a>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-10 col-sm-6 col-xs-12">
            <button class="btn btn-success save" data-toggle="tooltip" title="Guardar"> Guardar</button>
            <a class="btn btn-danger m-close" data-toggle="tooltip" title="Cancelar"> Cancelar</a>
            <input class="b-fase" type="hidden" name="id" value="{if isset($id)}{$id}{/if}" />
        </div>
    </div>
    </form>
</div>