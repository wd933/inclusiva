<script src="{$baseUrl}public/plugins/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
    <section class="content-header" style="margin-bottom: 20px">
        <ol class="breadcrumb">
            <li class="active"><a href="{$baseUrl}admin"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"><a href="{$baseUrl}admin/home/panel"><i class="fa fa-address-card-o"></i> Información del home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <form action="{$baseUrl}admin/home/accion_panel" class="form" method="POST">
                <div class="col-md-12">
                    <div class="box box-success box-solid">
                        <div class="box-header">
                            <h3 class="box-title">{$titulo}</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="col-md-12">
                                <span class="response"></span>
                                <h4>Textos para : <b>¿Qué nos distigue?</b></h4>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Garantía y alta estética:</label>
                                    <textarea class="form-control" rows="5" name="garantia">{if isset($garantia)}{$garantia['valor']}{/if}</textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Infraestructura:</label>
                                    <textarea class="form-control" rows="5" name="infraestructura">{if isset($infraestructura)}{$infraestructura['valor']}{/if}</textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Calidad de servicio:</label>
                                    <textarea class="form-control" rows="5" name="calidad_servicio">{if isset($calidad_servicio)}{$calidad_servicio['valor']}{/if}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <legend></legend>
                            </div>

                            <div class="col-md-12">
                                <h4>Texto para : <b>Conoce nuestras especialidades</b></h4>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Texto:</label>
                                    <textarea class="form-control" rows="5" name="especialidades">{if isset($especialidades)}{$especialidades['valor']}{/if}</textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar"> Guardar</button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>       

    </section>

</div>

