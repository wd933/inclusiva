<div class="row">                        
    <form action="{$baseUrl}admin/carrusel_chamba/accion" class="form" method="POST">
    <div class="response"></div>

    <div class="col-md-12">
        <label><span class="text-red">[*]</span> Imagen:</label>
        <div class="form-group">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    {if isset($imagen)}
                        {if $imagen != ''}
                            <img src="{$baseUrl}assets/images/carrusel/{$imagen}" alt="...">
                        {else}
                            <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                        {/if}
                    {else}
                        <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                    {/if}                    
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                <div>
                    <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen"></span>
                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-10 col-sm-6 col-xs-12">
            <button class="btn btn-success save" data-toggle="tooltip" title="Guardar"> Guardar</button>
            <a class="btn btn-danger m-close" data-toggle="tooltip" title="Cancelar"> Cancelar</a>
            <input class="b-fase" type="hidden" name="id" value="{if isset($id)}{$id}{/if}" />
        </div>
    </div>
    </form>
</div>