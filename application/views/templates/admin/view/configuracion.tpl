<div class="content-wrapper">
    <section class="content-header" style="margin-bottom: 30px">
        <ol class="breadcrumb">
            <li class="active"><a href="{$baseUrl}admin/home"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"><a href="{$baseUrl}admin/configuracion/panel"><i class="fa fa-cogs"></i> Configuracion</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="response"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Configuracion de la Pagina</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{$baseUrl}admin/configuracion/accion_pagina" class="form" method="POST" autocomplete="off">
                        <span class="response"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Titulo de la página:</label>
                                    <input type="text" class="form-control" name="proyecto" value="{if isset($proyecto_nombre['valor'])}{$proyecto_nombre['valor']}{/if}" />
                                </div>
                            </div>                        

                            <div class="col-md-6">
                                <label><span class="text-red">[*]</span> Logo Principal: </label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;background: #ccc;">
                                            {if $logo['valor'] != ''}
                                                <img src="{$baseUrl}assets/images/{$logo['valor']}" alt="...">
                                            {else}
                                                <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                                            {/if}                                           
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                                        <div>
                                            <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="logo"></span>
                                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label><span class="text-red">[*]</span> Favicon: <small class="text-red">formato ico 16 x 16px</small></label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;background: #ccc;">
                                            {if $favicon['valor'] != ''}
                                                <img src="{$baseUrl}assets/images/{$favicon['valor']}" alt="...">
                                            {else}
                                                <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                                            {/if}  
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                                        
                                        <div>
                                            <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="favicon"></span>
                                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 text-center">
                                <legend></legend>
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
                                <p><span class="text-red tex">[*]</span> Son campos obligatorios</p>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Redes Sociales</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{$baseUrl}admin/configuracion/accion_redes_sociales" class="form" method="POST" autocomplete="off">
                        <span class="response"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Facebook:</label>
                                    <input type="text" class="form-control" name="facebook" value="{if isset($facebook['valor'])}{$facebook['valor']}{/if}" />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Youtube:</label>
                                    <input type="text" class="form-control" name="youtube" value="{if isset($youtube['valor'])}{$youtube['valor']}{/if}" />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Instagram:</label>
                                    <input type="text" class="form-control" name="instagram" value="{if isset($instagram['valor'])}{$instagram['valor']}{/if}" />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Whatsapp:</label>
                                    <input type="text" class="form-control" name="whatsapp" value="{if isset($whatsapp['valor'])}{$whatsapp['valor']}{/if}" />
                                </div>
                            </div>                        

                            <div class="col-md-12 text-center">
                                <legend></legend>
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
                                <p><span class="text-red tex">[*]</span> Son campos obligatorios</p>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Datos de Contacto</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{$baseUrl}admin/configuracion/accion_datos_contacto" class="form" method="POST" autocomplete="off">
                        <span class="response"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Telefono:</label>
                                    <input type="text" class="form-control" name="telefono" value="{if isset($telefono['valor'])}{$telefono['valor']}{/if}" />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Correo:</label>
                                    <input type="text" class="form-control" name="correo" value="{if isset($correo['valor'])}{$correo['valor']}{/if}" />
                                </div>
                            </div>  

                            {* <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Dirección:</label>
                                    <input type="text" class="form-control" name="direccion" value="{if isset($direccion['valor'])}{$direccion['valor']}{/if}" />
                                </div>
                            </div> *}

                            {* <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Horario:</label>
                                    <input type="text" class="form-control" name="horario" value="{if isset($horario['valor'])}{$horario['valor']}{/if}" />
                                </div>
                            </div> *}

                            {* <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Mapa de google:</label>
                                    <textarea class="form-control" rows="4" name="googlemap">{if isset($googlemap)}{$googlemap['valor']}{/if}</textarea>
                                </div>
                            </div> *}

                                                  

                            <div class="col-md-12 text-center">
                                <legend></legend>
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
                                <p><span class="text-red tex">[*]</span> Son campos obligatorios</p>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Otros datos</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{$baseUrl}admin/configuracion/accion_otros_datos" class="form" method="POST" autocomplete="off">
                        <span class="response"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="text-red">[*]</span> Video para interna de Inclusiva:</label>
                                    <input type="text" class="form-control" name="video_inclusiva" value="{if isset($video_inclusiva['valor'])}{$video_inclusiva['valor']}{/if}" />
                                </div>
                            </div>

                            <div class="col-md-12 text-center">
                                <legend></legend>
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
                                <p><span class="text-red tex">[*]</span> Son campos obligatorios</p>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Imagenes de Nuestra Chamba</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{$baseUrl}admin/configuracion/accion_imagenes_chamba" class="form" method="POST" autocomplete="off">
                        <span class="response"></span>
                            <div class="col-md-6">
                                <label><span class="text-red">[*]</span> Imagen lateral: </label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="background: #ccc;">
                                            {if $imagen_chamba_1['valor'] != ''}
                                                <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_1['valor']}" alt="...">
                                            {else}
                                                <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                                            {/if}                                           
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                                        <div>
                                            <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen_chamba_1"></span>
                                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <label><span class="text-red">[*]</span> Imagen derecha 1:</label>
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="background: #ccc;">
                                                {if $imagen_chamba_2['valor'] != ''}
                                                    <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_2['valor']}" alt="...">
                                                {else}
                                                    <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                                                {/if}  
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                                            
                                            <div>
                                                <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen_chamba_2"></span>
                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label><span class="text-red">[*]</span> Imagen derecha 2:</label>
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="background: #ccc;">
                                                {if $imagen_chamba_3['valor'] != ''}
                                                    <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_3['valor']}" alt="...">
                                                {else}
                                                    <img src="{$baseUrl}assets/images/no-imagen.jpg" alt="...">
                                                {/if}  
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" ></div>
                                            
                                            <div>
                                                <span class="btn btn-success btn-file"><span class="fileinput-new">Seleccionar Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen_chamba_3"></span>
                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 text-center">
                                <legend></legend>
                                <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
                                <p><span class="text-red tex">[*]</span> Son campos obligatorios</p>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

