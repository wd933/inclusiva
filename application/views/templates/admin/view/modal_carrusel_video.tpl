<div class="row">                        
    <form action="{$baseUrl}admin/carrusel_video/accion" class="form" method="POST">
    <div class="response"></div>
    <div class="col-md-12">
        <div class="form-group">
            <label><span class="text-red">[*]</span> Nombres:</label>
            <input type="text" class="form-control"  name="url" value="{if isset($url)}{$url}{/if}" />
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="col-md-10 col-sm-6 col-xs-12">
            <button class="btn btn-success save" data-toggle="tooltip" title="Guardar"> Guardar</button>
            <a class="btn btn-danger m-close" data-toggle="tooltip" title="Cancelar"> Cancelar</a>
            <input class="b-fase" type="hidden" name="id" value="{if isset($id)}{$id}{/if}" />
        </div>
    </div>
    </form>
</div>