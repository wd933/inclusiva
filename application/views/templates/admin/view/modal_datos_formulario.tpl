                    <div class="box-body">
                        <div style="padding-left: 20px; padding-right: 20px;" class="form-group has-feedback">
                            <div class=" table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <td><strong> Nombres y Apellidos </strong></td>
                                        <td>{$nombre}</td>
                                    </tr>
                                    <tr>
                                        <td><strong> Telefono </strong></td>
                                        <td>{$telefono}</td>
                                    </tr>
                                    <tr>
                                        <td><strong> Email </strong></td>
                                        <td>{$email}</td>
                                    </tr>
                                    <tr>
                                        <td><strong> Fecha Registro</strong></td>
                                        <td>{$fecha}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center"><strong> Mensaje </strong></td> 
                                    </tr>
                                    <tr>
                                        <td colspan="2">{$mensaje}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12 col-sm-6 col-xs-12 text-center">
                                <a class="btn btn-danger m-close" data-type="4"> Cerrar</a>
                            </div>
                        </div>
                    </div>