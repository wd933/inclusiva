<body style="background: #F5F5F5;">
    <div class="login-box">    
        <div class="login-box-body text-center" style="background: #fff;">
            <span>
                <img src="{$baseUrl}assets/images/logo-inclusiva.png" class="img-responsive" style="width: 100px;display: initial;">
            </span>
            <legend class="text-center"><strong>Panel de acceso</strong></legend>
            <form action="{$baseUrl}admin/login/ingresar" method="POST" class="form">
                <span class="response"></span>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control input-test" name="usuario" placeholder="Usuario">
                    </div>       
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                        <input type="password" class="form-control input-test" name="password" placeholder="Contraseña">
                    </div>       
                </div>

                <div class="social-auth-links text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-comenzar save"> Ingresar</button>   
                        </div>
                    </div>                   
                </div>
            </form>
        </div>
    </div>
</body>