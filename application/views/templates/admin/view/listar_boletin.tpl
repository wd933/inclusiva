<div class="content-wrapper">
    <section class="content-header">
        <ol class="breadcrumb" style="margin-bottom: 10px">
            <li class="active"><a href="{$baseUrl}admin/home"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"><a href="{$baseUrl}admin/boletin/listar"><i class="fa fa-list"></i> Listado de Boletines</a></li>
        </ol>
        <a data-url="{$baseUrl}admin/boletin/agregar" class="btn btn-success m-add"> Nuevo</a>
    </section>
    <section class="content">
        <div class="response"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">{$titulo}</h3>
                    </div>
                    <div class="box-body">

                        <div class="direct-chat-msg table-responsive">
                            <table class="table table-bordered table-hover example-table" style="text-align: center">
                                <thead>
                                    <tr class="tr-lista" style="text-align: center;">
                                        <th style="width: 35px; text-align: center;">N°</th>
                                        <th style="text-align: center">Imagen</th>
                                        <th style="text-align: center">Archivo</th>
                                        <th style="text-align: center">Accion</th>
                                    </tr>
                                </thead>
                                   
                                {if isset($lista)}
                                {foreach $lista as $l}
                                        <tr class="resultado">
                                            <td>{$l.numero}</td>
                                            <td>
                                                <img src="{$baseUrl}assets/images/boletines/{$l.imagen}" style="height:60px;">
                                            </td>
                                            <td class="text-center">
                                                <a href="{$baseUrl}assets/pdf/{$l.pdf}" target="_blank"><img src="{$baseUrl}assets/images/extras/pdf.png" class="img-responsive" style="width: 45px;display: initial;"></a>
                                            </td>
                                            <td>{$l.accion}</td>
                                        </tr>                                       
                                {/foreach}
                                {/if}
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<div id="modalContenido" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="tituloModal" class="modal-title"></h4>
            </div>
            <div id="modalContenidoBody" class="modal-body">
            </div>
        </div>
    </div>
</div>
