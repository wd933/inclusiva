<div class="row">
    <form action="{$base_url}admin/usuario/accion_password" class="form" method="POST">
    <div class="response"></div>

    <div class="col-md-12">
        <div class="form-group">
            <label><span class="text-red">[*]</span> Nueva Password:</label>
            <input type="password" class="form-control"  name="password" value="{if isset($password)}{$password}{/if}" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label><i class="fa fa-asterisk fa-xs text-red"></i> Confirmar Password:</label>
            <input type="password" class="form-control"  name="repassword" value="{if isset($repassword)}{$repassword}{/if}" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-12 col-sm-6 col-xs-12 text-center">
            <button class="btn btn-success save" data-toggle="tooltip" title="Guardar">Guardar</button>
            <a class="btn btn-danger m-close" data-toggle="tooltip" title="Cancelar">Cancelar</a>
            <input class="b-fase" type="hidden" name="id" value="{if isset($id)}{$id}{/if}" />
        </div>
    </div>
    </form>
</div>