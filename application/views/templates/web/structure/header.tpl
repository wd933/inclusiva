<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{$title_page}</title>    
<link rel="icon" href="{$baseUrl}assets/images/{if isset($_favicon)}{$_favicon}{/if}" type="image/icon" />
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="{$baseUrl}assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{$baseUrl}assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{$baseUrl}assets/css/style.css?v={$time}">

<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" async>

<script> var baseUrl = "{$baseUrl}";</script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>