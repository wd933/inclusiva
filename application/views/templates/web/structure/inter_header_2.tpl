<body data-spy="scroll" data-target=".navbar-fixed-top">

    <div class="header-top clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 left">
                    <span class="header-info __a1">
                        <i class="fa fa-map-marker" aria-hidden="true"></i> {if isset($_direccion)}{$_direccion}{/if}</span>
                    <div class="header-separator"></div>
                    <span class="header-info __a2">
                        <i class="fa fa-envelope" aria-hidden="true"></i> {if isset($_correo)}{$_correo}{/if}</span>
                    <div class="header-separator"></div>
                    <span class="header-info">
                        <i class="fa fa-calendar" aria-hidden="true"></i> {if isset($_horario)}{$_horario}{/if}</span>
                </div>

                <div class="col-lg-2 col-md-2 text-right">
                    <span>
                        <a href="{$_facebook}" target="_blank" class="redes"><i class="fa fa-facebook"></i></a>
                        <a href="{$_youtube}" target="_blank" class="redes"><i class="fa fa-youtube-play"></i></a>
                        <a href="#" data-number="{$_whatsapp}" class="redes send-wsp"><i class="fa fa-whatsapp"></i></a>
                        <a href="{$_instagram}" target="_blank" class="redes"><i class="fa fa-instagram"></i></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    
    <nav class="navbar navbar-default  nav-cda">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand smoth-scroll" href="{$baseUrl}">
                        <img src="{$baseUrl}assets/img/logo.png" alt="logo" style="width: 200px;">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="main-nav-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{$baseUrl}">INICIO</a></li>
                        <li class="{if isset($active2)}{$active2}{/if}"><a href="{$baseUrl}nosotros">NOSOTROS</a></li>
                        <li class="dropdown">
                            <a class="" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ESPECIALIDADES <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{$baseUrl}endodoncia">ENDODONCIA</a></li>
                                <li><a href="{$baseUrl}odontologia-estetica">ODONTOLOGÍA ESTÉTICA</a></li>
                                <li><a href="{$baseUrl}odontopediatria">ODONTOPEDIATRÍA</a></li>
                                <li><a href="{$baseUrl}ortodoncia">ORTODONCIA</a></li>
                                <li><a href="{$baseUrl}periodoncia-implantologia-cirugia-oral">PERIODONCIA, IMPLANTOLOGÍA Y CIRUGÍA ORAL</a></li>
                                <li><a href="{$baseUrl}rehabilitacion-oral">REHABILITACIÓN ORAL</a></li>
                            </ul>
                        </li>
                        <li class="{if isset($active4)}{$active4}{/if}"><a href="{$baseUrl}testimonios">TESTIMONIOS</a></li>
                        <li class="{if isset($active5)}{$active5}{/if}"><a href="{$baseUrl}programas-tv">PROGRAMAS TV</a></li>
                        <li class="{if isset($active6)}{$active6}{/if}"><a href="{$baseUrl}cita-en-linea">CITA EN LINEA</a></li>                       
                    </ul>
                </div>
            </div>
        </nav> 
