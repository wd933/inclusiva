<section class="section-form-contact">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 text-center mb-50">
                {* <h2><img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" /> <span class="multicolor">¿Un café?</span> <img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" /></h2> *}
                <img src="{$baseUrl}assets/images/extras/uncafe.png" class="img-responsive img-aqui" alt="imagen" />
            </div>
            <form class="form-horizontal form" action="{$baseUrl}registro/accion_contacto" autocomplete="off" method="POST">
                <span class="response"></span>
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-12 col-md-4" for="nombre" style="max-width: 195px;">Nombres y Apellidos:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control input-icl" name="nombre" id="nombre" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="telefono">Teléfono:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-icl" name="telefono" id="telefono" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-5" for="email" style="max-width: 195px;">Correo:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control input-icl" name="email" id="email" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="mensaje">Mensaje:</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="4" name="mensaje"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <button class="btn btn-primary btn-contact save" title="Enviar"> Enviar</button>
                </div>                
            </form>
        </div>
    </div>
</section>
<footer id="footer" class="">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 text-center mt-40">
                <img src="{$baseUrl}assets/images/extras/aqui-final.png" class="img-responsive img-aqui" alt="imagen" />
            </div>

            <div class="col-md-offset-1 col-md-10 text-center">
                <p class="correo-footer">{if isset($_correo)}{$_correo}{/if}</p>
                <p class="mt-20">
                    <a href="{if isset($_facebook)}{$_facebook}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/facebook.png" alt="imagen" class="red-footer" /></a>
                    <a href="{if isset($_instagram)}{$_instagram}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/instagram.png" alt="imagen" class="red-footer" /></a>
                    <a href="{if isset($_linkedin)}{$_linkedin}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/linkedin.png" alt="imagen" class="red-footer" /></a>
                    <a href="{if isset($_twitter)}{$_twitter}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/twitter.png" alt="imagen" class="red-footer" /></a>
                </p>                
            </div>

            <div class="col-md-offset-1 col-md-10 mt-40">
                <div class="pull-left">
                    <img src="{$baseUrl}assets/images/logo-inclusiva.png" class="img-responsive logo-footer-2" alt="logo inclusiva" />    
                </div>
                <div class="pull-right">
                    <img src="{$baseUrl}assets/images/extras/logo-BFTW.png" class="d-inline-block img-responsive logoempresa-footer-2" alt="imagen" />
                    <img src="{$baseUrl}assets/images/extras/logoempresab.png" class="d-inline-block img-responsive logoempresa-footer-2" alt="imagen" />
                </div>
                
            </div>
            
                {* <div class="col-md-12 text-center visible-xs">
                    <img src="{$baseUrl}assets/images/logo-inclusiva.png" class="img-responsive logo-footer-2" alt="logo inclusiva" />
                </div>

                <div class="col-md-offset-1 col-md-10 col-xs-12 col-xs-offset-0 div-footer">
                    <div class="flex-footer pos-relative hidden-xs">
                        <img src="{$baseUrl}assets/images/logo-inclusiva.png" class="img-responsive logo-footer" alt="logo inclusiva" />
                    </div>
                    
                    
                    <div class="flex-footer">
                        <h4><img src="{$baseUrl}assets/images/extras/extra-10.png" alt="imagen" /> <span class="multicolor">Aquí nos puedes encontrar</span> <img src="{$baseUrl}assets/images/extras/extra-11.png" alt="imagen" /></h4>
                        <p><i class="fa fa-envelope" style="margin-right: 5px;"></i> <span>{if isset($_correo)}{$_correo}{/if}</span></p>
                        <p>
                            <p>Redes Sociales:</p>
                            <p>
                                <a href="{if isset($_facebook)}{$_facebook}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/facebook.png" alt="imagen" /></a>
                                <a href="{if isset($_instagram)}{$_instagram}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/instagram.png" alt="imagen" /></a>
                                <a href="{if isset($_linkedin)}{$_linkedin}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/linkedin.png" alt="imagen" /></a>
                                <a href="{if isset($_twitter)}{$_twitter}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/twitter.png" alt="imagen" /></a>
                            </p>
                        </p> 
                    </div>

                </div> *}
            
            
        </div>
    </div>
</footer>

