<body>
	<div class="container hidden-xs">
  		<nav class="navbar navbar-default">
    		<div class="container-fluid">
      			<div class="navbar-header">
        			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
			          	<span class="sr-only">Toggle navigation</span>
			          	<span class="icon-bar"></span>
			          	<span class="icon-bar"></span>
			          	<span class="icon-bar"></span>
        			</button>
              		<a class="navbar-brand" href="{$baseUrl}"><img src="{$baseUrl}assets/images/{if isset($_logo)}{$_logo}{/if}" alt="Logo Inclusiva" /></a>
      			</div>

      			<div id="navbar" class="navbar-collapse collapse">
        			<ul class="nav navbar-nav navbar-left">
          				<li>
          					<button type="button" class="bars-menu" data-toggle="modal" data-target="#myModalMenuDesktop">
					          	<span><i class="fa fa-bars"></i></span>
		        			</button>
          				</li>          				
        			</ul>
        			<ul class="nav navbar-nav navbar-right navbar-social">
			          	<li><a href="{if isset($_facebook)}{$_facebook}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/facebook.png" alt="imagen" class="red-header" /></a></li>
			          	<li><a href="{if isset($_instagram)}{$_instagram}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/instagram.png" alt="imagen" class="red-header" /></a></li>
			          	<li><a href="{if isset($_linkedin)}{$_linkedin}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/linkedin.png" alt="imagen" class="red-header" /></a></li>
			          	<li><a href="{if isset($_twitter)}{$_twitter}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/twitter.png" alt="imagen" class="red-header" /></a></li>
			          	<div class="text-right">
			          		<img src="{$baseUrl}assets/images/extras/logo-BFTW.png" class="d-inline-block img-responsive logo-certific" alt="imagen" />
			          		<img src="{$baseUrl}assets/images/extras/logoempresab.png" class="d-inline-block img-responsive logo-certific" alt="imagen" />
			          	</div>
        			</ul>
      			</div>
    		</div>
  		</nav>
	</div>

	<div class="container visible-xs">
		{* <div class="col-md-12 "> *}
			<div class="col-xs-4 col-md-4 mt-10">
				<button type="button" class="bars-menu bars-menu-mobile" data-toggle="modal" data-target="#myModalMenuDesktop"><span><i class="fa fa-bars"></i></span></button>
			</div>
			<div class="col-xs-4 col-md-4 mt-30">
				<a class="" href="{$baseUrl}">
					<img src="{$baseUrl}assets/images/{if isset($_logo)}{$_logo}{/if}" class="img-responsive" alt="Logo Inclusiva" />
				</a>
			</div>
			<div class="col-xs-4 col-md-4 mt-30 no-padding">
				<ul class="navbar-social-mobile">
			          	<li><a href="{if isset($_facebook)}{$_facebook}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/facebook.png" alt="imagen" /></a></li>
			          	<li><a href="{if isset($_instagram)}{$_instagram}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/instagram.png" alt="imagen" /></a></li>
			          	<li><a href="{if isset($_linkedin)}{$_linkedin}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/linkedin.png" alt="imagen" /></a></li>
			          	<li><a href="{if isset($_twitter)}{$_twitter}{/if}" target="_blank"><img src="{$baseUrl}assets/images/extras/twitter.png" alt="imagen" /></a></li>
			          	<div class="text-right">
			          		<img src="{$baseUrl}assets/images/extras/logo-BFTW.png" class="d-inline-block img-responsive logo-certific-mobile" alt="imagen" />
							<img src="{$baseUrl}assets/images/extras/logoempresab.png" class="d-inline-block img-responsive logo-certific-mobile" alt="imagen" />
			          	</div>
        			</ul>
			</div>
			{* <ul class="ul-menu-mobile">
				<li>
					<button type="button" class="bars-menu bars-menu-mobile" data-toggle="modal" data-target="#myModalMenuDesktop"><span><i class="fa fa-bars"></i></span></button>
				</li>
				<li>
					<a class="navbar-brand" href="{$baseUrl}"><img src="{$baseUrl}assets/images/{if isset($_logo)}{$_logo}{/if}" alt="Logo Inclusiva" /></a>
				</li>
				<li>
				</li>
			</ul> *}
		{* </div> *}
	</div>

    
    <!-- Modal -->
	<div class="modal left fade" id="myModalMenuDesktop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Menú</h4>
				</div>

				<div class="modal-body">
					<ul class="navbar-inclusiva">
						<li>
							<a href="{$baseUrl}">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />Hola<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}inclusiva">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />Inclusiva<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}nuestra-chamba">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />Nuestra Chamba<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}la-base-what">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />La Base, What?<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}resultados-power">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />Resultados Power<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}caseritos">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />Caseritos<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
						<li>
							<a href="{$baseUrl}que-novelas">
								<img src="{$baseUrl}assets/images/extras/extra-8.png" alt="imagen" class="img-menu-1" />¿Qué Novelas?<img src="{$baseUrl}assets/images/extras/extra-9.png" alt="imagen" class="img-menu-2" />
							</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
	
