<section class="section-inclusiva">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center mt-60">
            <img src="{$baseUrl}assets/images/extras/resultados-final.png" class="img-responsive" alt="imagen" style="display: initial;width: 90%;" />
        </div>

        <div class="col-md-offset-1 col-md-10 text-center">
            <img src="{$baseUrl}assets/images/extras/comobuscamos.png" class="img-responsive" alt="imagen" style="display: initial;width: 90%;" />
        </div>
    </div>
</section>

<section class="section-inclusiva">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding">
            <div id="myCarouselFrase1" class="carousel slide" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#myCarouselFrase1" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarouselFrase1" data-slide-to="1"></li>
                    <li data-target="#myCarouselFrase1" data-slide-to="2"></li>
                    <li data-target="#myCarouselFrase1" data-slide-to="3"></li>
                    <li data-target="#myCarouselFrase1" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{$baseUrl}assets/images/frases/mejoradeingresos-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/impulsocrecimiento-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/fortalecimiento-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/disminuciondecostos-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/calidaddevidapng-2.png" class="img-responsive" alt="imagen" />
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-inclusiva">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <img src="{$baseUrl}assets/images/extras/empresa2.png" class="img-responsive" alt="imagen" style="display: initial;width: 50%;" />
        </div>
    </div>
</section>

<section class="section-inclusiva">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding">
            <div id="myCarouselFrase2" class="carousel slide" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#myCarouselFrase2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarouselFrase2" data-slide-to="1"></li>
                    <li data-target="#myCarouselFrase2" data-slide-to="2"></li>
                    <li data-target="#myCarouselFrase2" data-slide-to="3"></li>
                    <li data-target="#myCarouselFrase2" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{$baseUrl}assets/images/frases/exploraciondenuevos-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/apuesta-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/crecimiento-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/mejoralacompeti-2.png" class="img-responsive" alt="imagen" />
                    </div>
                    <div class="item">
                        <img src="{$baseUrl}assets/images/frases/mejoralarelacionyvinculo-2.png" class="img-responsive" alt="imagen" />
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>