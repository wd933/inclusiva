<section class="section-nuestros-clientes">
    <div class="text-center">
        <img src="{$baseUrl}assets/images/extras/caseritos-full.png" class="img-responsive img-caserito" alt="imagen" />
    </div>
    <div class="container">
        <div class="col-md-offset-1 col-md-10 flex-center">
            {if !empty($clientes)}
                {foreach $clientes as $c}
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 text-center mt-40">
                        <img src="{$baseUrl}assets/images/clientes/{$c.logo}" class="img-responsive" alt="{$c.logo}">
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</section>
<section class="section-internal-2">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <div class="col-md-12 text-center mb-10">
                <img src="{$baseUrl}assets/images/extras/miraloque.png" class="img-responsive" alt="imagen" style="width: 90%;display: initial;" />
            </div>
        </div>
    </div>
</section>

<section class="section-inclusiva">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding carrusel-client-caserito">
            <div id="myCarouselCliente" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    {if !empty($c_clientes)}
                        {foreach $c_clientes as $cc}
                            {if $cc['item'] == 0}
                                <div class="item active">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive img-full" alt="{$cc['imagen']}" />
                                </div>
                            {else}
                                <div class="item">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive img-full" alt="{$cc['imagen']}" />
                                </div>
                            {/if}
                        {/foreach}
                    {/if}                    
                </div>
                <a class="left carousel-control" href="#myCarouselCliente" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarouselCliente" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section-internal">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <div class="col-md-12 text-center mb-40">
                <img src="{$baseUrl}assets/images/extras/nuestrosocio.png" class="img-responsive img-nuestro-equipo" alt="imagen" />
            </div>
            {* <h4 class="title-team">
                <img src="{$baseUrl}assets/images/extras/extra-10.png" alt="imagen">
                Nuestros socios
                <img src="{$baseUrl}assets/images/extras/extra-11.png" alt="imagen">
            </h4> *}
            <h2 class="texto-socios">
                Contamos con una sólida red de partners en Latinoamérica que hacen posible replicar, complementar y potencializar los servicios prestados por Inclusiva en los países que atendemos: Bolivia, Brasil, Chile, Argentina, Colombia, Ecuador, México y Perú.
            </h2>
            {* <div class="text-center">
                <img src="{$baseUrl}assets/images/extras/flecha.png" style="width: 23%" alt="imagen">
            </div> *}
        </div>
    </div>
</section>

<section class="section-nuestros-socios">
    <div class="container">
        <div class="col-md-offset-2 col-md-8 flex-center">
            {if !empty($socios)}
                {foreach $socios as $s}
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center mb-10">
                        {* <img src="{$baseUrl}thumbs/180/220/socios_{$s.logo}" class="img-responsive" style="display:inline-block" alt="{$c.logo}"> *}
                        <img src="{$baseUrl}assets/images/socios/{$s.logo}" class="img-responsive" style="display:inline-block" alt="{$c.logo}">
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</section>