<section class="banner-video">
	<div class="container">
        <div class="col-xs-12 col-xs-offset-0 col-md-offset-2 col-md-8">
        	<div class="contenedor-youtube video-inclusive">
                <div class="embed-responsive embed-responsive-16by9" id="section_1_2">
                    <div class="contenedor-youtube">
                        <div class="reproductor-youtube reproductor-youtube-2" data-id="{$video_inclusiva}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="banner-quienes-somos">
	<div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 text-center">
                <img src="{$baseUrl}assets/images/extras/quienesomos.png" class="img-responsive img-quienes" alt="imagen" />
                <h2 class="hidden-xs">
                	Somos una consultora experta en la Base de la Pirámide,<br>
                	segmentos vulnerables y emergentes <br>
                	de Perú y Latinoamérica
                </h2>
                <h2 class="visible-xs">
                    Somos una consultora experta en<br>la Base de la Pirámide,
                    segmentos vulnerables y emergentes
                    de Perú y Latinoamérica
                </h2>
            </div>
            <div class="col-md-12 text-center">
            	<img src="{$baseUrl}assets/images/extras/portada-quienes-somos.png" class="img-responsive" alt="imagen" style="width: 90%;" />
            </div>
        </div>
</section>


<section class="section-form-portada">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 mb-40">
            <div class="col-md-12 text-center">
                <img src="{$baseUrl}assets/images/extras/nuestroequipo2.png" class="img-responsive img-nuestro-equipo" alt="imagen" />
            </div>
        </div>

        <div class="col-md-offset-1 col-md-10 flex-top">
            {if !empty($c_equipo)}
                {foreach $c_equipo as $cc}
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-40">
                        <img src="{$baseUrl}thumbs/210/210/equipo_{$cc['imagen']}" class="img-responsive img-circle img-team" alt="{$cc['nombre']}">
                        <h3 class="team-name">{$cc['nombre']}</h3>
                        <p class="team-description">{$cc['cargo']}</p>
                    </div>
                {/foreach}
            {/if}            
        </div>
    </div>
</section>

<section class="section-form-portada">
    <div class="container">
        <div class="col-md-12">
            <div class="mt-10 mb-10 text-center">
                <img src="{$baseUrl}assets/images/extras/diferencia.png" class="img-responsive" alt="imagen" style="display: initial;height: 130px;" />
            </div>
            <div class="mt-20 mb-20">
                <div class="col-md-4 mb-20">
                    <img src="{$baseUrl}assets/images/extras/diferencia-experiencia.png" class="img-responsive" alt="imagen" style="display: initial;" />                    
                </div>
                <div class="col-md-4 mb-20">
                    <img src="{$baseUrl}assets/images/extras/diferencia-imagen2.png" class="img-responsive" alt="imagen" style="display: initial;" />                    
                </div>
                <div class="col-md-4 mb-20">
                    <img src="{$baseUrl}assets/images/extras/diferencia-imagen3.png" class="img-responsive" alt="imagen" style="display: initial;" />                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="mt-10 mb-10 text-center">
                <img src="{$baseUrl}assets/images/extras/empresab.png" class="img-responsive" alt="imagen" style="display: initial;height: 130px;" />    
            </div>
            <div class="mt-20 mb-20">
                <div class="col-md-6 mb-20">
                    <img src="{$baseUrl}assets/images/extras/empresab-que-es.png" class="img-responsive" alt="imagen" style="display: initial;" />
                </div>
                <div class="col-md-6 mb-20">
                    <img src="{$baseUrl}assets/images/extras/empresab-porque.png" class="img-responsive" alt="imagen" style="display: initial;" />
                </div>
            </div>
        </div>

        {* <div class="col-md-offset-1 col-md-10">
            <div class="col-md-6">
                <img src="{$baseUrl}assets/images/extras/quenosdiferencia.png" class="img-responsive" alt="imagen" style="display: initial;" />

                <div>
                    <img src="{$baseUrl}assets/images/no-imagen.jpg" class="img-responsive" alt="imagen" style="display: initial;" />                    
                </div>
            </div>

            <div class="col-md-6">
                <img src="{$baseUrl}assets/images/extras/somosempresab.png" class="img-responsive" alt="imagen" style="display: initial;" />

                <div>
                    <img src="{$baseUrl}assets/images/no-imagen.jpg" class="img-responsive" alt="imagen" style="display: initial;" />                    
                </div>
            </div>

            <div class="col-md-12 text-center">
                <img src="{$baseUrl}assets/images/extras/quenosdiferencia.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-20">
                <h4 class="h4-diferencia">Contamos con más de 15 años de experiencia en distintos países de la Región desde diversos sectores económicos y en diferentes contextos</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-unica">Única consultora experta en la BdP en Perú</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-compromiso">Compromiso, conexión y convicción en lo que hacemos y el impacto que buscamos generar.</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-12 text-center mt-60 no-padding-xs">
                <img src="{$baseUrl}assets/images/extras/somosempresab.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-empresa">Las Empresas B son aquellas que miden su impacto social y ambiental y se comprometen de forma personal, institucional y legal a tomar decisiones considerando las consecuencias de sus acciones a largo plazo en la comunidad y el medioambiente.</h4>
            </div>

            <div class="col-md-offset-1 col-md-10 text-center mt-60">
                <div class="col-md-3">
                    <img src="{$baseUrl}assets/images/extras/logoempresab.png" class="img-responsive img-certifica" alt="imagen" />
                </div>
                <div class="col-md-9 text-left mt-20">
                    <h4 class="h4-certificada">¡Estamos orgullosas de ser una <span style="color: #fff;">Empresa B</span> certificada, en cada proyecto generamos un triple impacto positivo: económico, social y ambiental!</h4>    
                </div>                
            </div>
        </div> *}
    </div>
</section>

<section class="section-form-portada">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="mb-30 text-center">
                <img src="{$baseUrl}assets/images/extras/carbono.png" class="img-responsive" alt="imagen" style="display: initial;max-height: 200px;" />
            </div>
        </div>
        <div class="col-md-offset-3 col-md-6">
            <p class="text-center color-white">
                Cómo Inclusiva hemos firmado un compromiso cero emisiones de carbono para el 2030 y la primera acción que implementamos para ello es nuestra alianza con Milpuj La Heredad, zona destinada a la conservación de la biodiversidad de los bosques secos de la región Amazonas, en Perú.
            </p>
        </div>
    </div>
</section>

<section class="section-internal">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <div style="width:59.6%;float:left;padding-right: 1.45%;">
                <img src="{$baseUrl}assets/images/extras/carbono-1.jpeg" class="img-responsive img-carbono-1" alt="carbono-1" style="display: initial;">
            </div>
            <div style="width:40.4%;float:left;">
                <div class="" style="padding-bottom: 4%;">
                    <img src="{$baseUrl}assets/images/extras/carbono-2.jpeg" class="img-responsive" alt="carbono-2">
                </div>
                <div class="" style="padding-bottom: 4%;">
                    <img src="{$baseUrl}assets/images/extras/carbono-3.jpeg" class="img-responsive" alt="carbono-2" style="display: initial;">    
                </div>
                <div class="">
                    <img src="{$baseUrl}assets/images/extras/carbono-4.jpeg" class="img-responsive" alt="carbono-2" style="display: initial;">    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-offset-1 col-md-10 text-center mt-20" style="color:#fff">
            <p class="mb-20">¡Estamos orgullosas de ser una empresa B certificada, en cada proyecto generamos un triple impacto positivo: económico, social y ambiental!</p>
            <p class="mb-20">La Empresa B aspira a ser la mejor empresa para el mundo y no solo del mundo.</p>
            <p class="mb-20">Si quieres saber más sobre el Certificación B entra aquí:<br>
                <a href="http://www.sistemab.org" target="_blank" style="color:#00C29A;">http://www.sistemab.org</a>
            </p>
        </div>
    </div>
</section>

<script>    
    (function() {        
        var v1 = document.getElementsByClassName("reproductor-youtube-2");
        for (var n = 0; n < v1.length; n++) {
            var p = document.createElement("div");
            p.innerHTML = labnolThumb2(v1[n].dataset.id);
            p.onclick = labnolIframe2;
            v1[n].appendChild(p);
        }
    })();
    function labnolThumb2(id) {
        return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
    }

    function labnolIframe2() {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=1");
        iframe.setAttribute("class", "embed-responsive-item");
        iframe.setAttribute("width", "485px");
        iframe.setAttribute("height", "250px");
        iframe.setAttribute("frameborder", "1");
        iframe.setAttribute("id", "youtube-iframe");
        this.parentNode.replaceChild(iframe, this);
    }
    function loadPopupVideo(){

    }
</script>