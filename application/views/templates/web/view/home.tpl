<section class="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 no-padding">
                <img src="{$baseUrl}assets/images/extras/portadahola.png" class="hidden-xs img-responsive mt-20" alt="imagen" />
                <img src="{$baseUrl}assets/images/extras/portadahola-mobile.png" class="hidden-md hidden-lg img-responsive mt-20" alt="imagen" />
            </div>
        </div>
    </div>
</section>

<section class="section-form-portada">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div id="myCarouselPortada" class="carousel slide" data-ride="carousel" data-interval="false">
                {if !empty($c_superior)}
                    <ol class="carousel-indicators">
                        {foreach $c_superior as $cs}
                            {if $cs['item'] == 0}
                                <li data-target="#myCarouselPortada" data-slide-to="{$cs['item']}" class="active"></li>
                            {else}
                                <li data-target="#myCarouselPortada" data-slide-to="{$cs['item']}"></li>
                            {/if}
                        {/foreach}
                    </ol>                    
                {/if}
                
                <div class="carousel-inner">
                    {if !empty($c_superior)}
                        {foreach $c_superior as $cs}
                            {if $cs['item'] == 0}
                                <div class="item active">
                                    <div class="img-portada-home">
                                        <img src="{$baseUrl}assets/images/carrusel/{$cs['imagen']}" class="img-responsive img-port" alt="{$cs['imagen']}" />
                                    </div>                        
                                </div>
                            {else}
                                <div class="item">
                                    <div class="img-portada-home">
                                        <img src="{$baseUrl}assets/images/carrusel/{$cs['imagen']}" class="img-responsive img-port" alt="{$cs['imagen']}" />
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>        
    </div>
</section>


<section class="home-video">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div id="myCarouselVideo" class="carousel slide" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    {if !empty($c_videos)}
                        {foreach $c_videos as $cv}
                            {if $cv['item'] == 0}
                                <li data-target="#myCarouselVideo" data-slide-to="{$cv['item']}" class="active"></li>
                            {else}
                                <li data-target="#myCarouselVideo" data-slide-to="{$cv['item']}"></li>
                            {/if}
                        {/foreach}
                    {/if}
                </ol>
                <div class="carousel-inner">
                    {if !empty($c_videos)}
                        {foreach $c_videos as $cv}
                            {if $cv['item'] == 0}
                                <div class="item active">
                                    <div class="contenedor-youtube">
                                        <div class="embed-responsive embed-responsive-16by9" id="section_1_2">
                                            <div class="contenedor-youtube">
                                                <div class="reproductor-youtube reproductor-youtube-2" data-id="{$cv['codigo']}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {else}
                                <div class="item">
                                    <div class="contenedor-youtube">
                                        <div class="embed-responsive embed-responsive-16by9" id="section_1_2">
                                            <div class="contenedor-youtube">
                                                <div class="reproductor-youtube reproductor-youtube-2" data-id="{$cv['codigo']}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}                    
                </div>
            </div>
        </div>        
    </div>
</section>

<section class="section-internal-2">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <div class="col-md-12 text-center mb-10">
                <img src="{$baseUrl}assets/images/extras/miraloque.png" class="img-responsive" alt="imagen" style="width: 90%;display: initial;" />
            </div>
        </div>
    </div>
</section>

<section class="section-inclusiva">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding">
            <div id="myCarouselCliente" class="carousel slide" data-ride="carousel" >
                <div class="carousel-inner">
                    {if !empty($c_clientes)}
                        {foreach $c_clientes as $cc}
                            {if $cc['item'] == 0}
                                <div class="item active">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive img-full" alt="{$cc['imagen']}" />
                                </div>
                            {else}
                                <div class="item">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive img-full" alt="{$cc['imagen']}" />
                                </div>
                            {/if}
                        {/foreach}
                    {/if}                    
                </div>
                <a class="left carousel-control" href="#myCarouselCliente" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarouselCliente" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

{*
<section class="section-form-noticia">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            {if !empty($c_noticias)}
                {foreach $c_noticias as $not}
                    <div class="col-md-12 mb-60">
                        <div class="img-noticia">
                            <img src="{$baseUrl}assets/images/noticias/{$not['imagen']}" class="img-responsive" alt="{$not['imagen']}" />
                        </div>
                        <div class="body-noticia">
                            <h3 class="">{$not['titulo']}</h3>
                            <p>{$not['subtitular']}</p>
                            <a href="{$not['url']}" target="_blank"><i class="fa fa-angle-right"></i> Lee mas</a>
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</section>
*}

<script>    
    (function() {        
        var v1 = document.getElementsByClassName("reproductor-youtube-2");
        for (var n = 0; n < v1.length; n++) {
            var p = document.createElement("div");
            p.innerHTML = labnolThumb2(v1[n].dataset.id);
            p.onclick = labnolIframe2;
            v1[n].appendChild(p);
        }
    })();
    function labnolThumb2(id) {
        return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
    }

    function labnolIframe2() {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=1");
        iframe.setAttribute("class", "embed-responsive-item");
        iframe.setAttribute("width", "485px");
        iframe.setAttribute("height", "250px");
        iframe.setAttribute("frameborder", "1");
        iframe.setAttribute("id", "youtube-iframe");
        this.parentNode.replaceChild(iframe, this);
    }
    function loadPopupVideo(){

    }
</script>