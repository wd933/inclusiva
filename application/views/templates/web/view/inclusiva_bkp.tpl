<section class="banner-video">
	<div class="container">
        <div class="col-xs-12 col-xs-offset-0 col-md-offset-2 col-md-8">
        	<div class="contenedor-youtube video-inclusive">
                <div class="embed-responsive embed-responsive-16by9" id="section_1_2">
                    <div class="contenedor-youtube">
                        <div class="reproductor-youtube reproductor-youtube-2" data-id="{$video_inclusiva}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="banner-quienes-somos">
	<div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 text-center">
                <img src="{$baseUrl}assets/images/extras/quienes-somos.png" class="img-responsive img-quienes" alt="imagen" />
                <h2 class="hidden-xs">
                	Somos una consultora experta en la Base de la Pirámide,<br>
                	segmentos vulnerables y emergentes <br>
                	de Perú y Latinoamérica
                </h2>
                <h2 class="visible-xs">
                    Somos una consultora experta en la Base de la Pirámide,
                    segmentos vulnerables y emergentes
                    de Perú y Latinoamérica
                </h2>
            </div>
            <div class="col-md-12 text-center">
            	<img src="{$baseUrl}assets/images/extras/portada-quienes-somos.png" class="img-responsive" alt="imagen" style="width: 90%;" />
            </div>
        </div>
</section>


<section class="section-form-portada">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 text-center">
                <h4 class="title-team"><img src="{$baseUrl}assets/images/extras/extra-10.png" alt="imagen" /> Nuestro equipo <img src="{$baseUrl}assets/images/extras/extra-11.png" alt="imagen" /></h4>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_karime-pavez.png" class="img-responsive img-circle img-team" alt="KARIME PAVEZ">
                <h3 class="team-name">KARIME PAVEZ</h3>
                <p class="text-muted">Directora y fundadora</p>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_valeria-soto.png" class="img-responsive img-circle img-team" alt="VALERIA SOTO">
                <h3 class="team-name">VALERIA SOTO</h3>
                <p class="text-muted">Analista de impacto</p>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_rubi-cubas.jpeg" class="img-responsive img-circle img-team" alt="RUBÍ CUBAS">
                <h3 class="team-name">RUBÍ CUBAS</h3>
                <p class="text-muted">Asistente de proyectos</p>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_edgar.jpg" class="img-responsive img-circle img-team" alt="EDGAR VELASQUEZ">
                <h3 class="team-name">EDGAR VELASQUEZ</h3>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_ine.jpeg" class="img-responsive img-circle img-team" alt="INE GABALDÓN">
                <h3 class="team-name">INE GABALDÓN</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mb-50">
                <img src="{$baseUrl}thumbs/210/210/equipo_jimena.jpg" class="img-responsive img-circle img-team" alt="JIMENA SANCHEZ">
                <h3 class="team-name">JIMENA SANCHEZ</h3>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center">
                <img src="{$baseUrl}thumbs/210/210/equipo_li.jpg" class="img-responsive img-circle img-team" alt="LI MINAYA">
                <h3 class="team-name">LI MINAYA</h3>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center">
                <img src="{$baseUrl}thumbs/210/210/equipo_mia.jpg" class="img-responsive img-circle img-team" alt="MIA HOYOS">
                <h3 class="team-name">MIA HOYOS</h3>
            </div>
            
            
        </div>
    </div>
</section>

<section class="section-form-portada">
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div class="col-md-12 text-center">
                <img src="{$baseUrl}assets/images/extras/quenosdiferencia.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-20">
                <h4 class="h4-diferencia">Contamos con más de 15 años de experiencia en distintos países de la Región desde diversos sectores económicos y en diferentes contextos</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-unica">Única consultora experta en la BdP en Perú</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-compromiso">Compromiso, conexión y convicción en lo que hacemos y el impacto que buscamos generar.</h4>
            </div>

            <div class="col-md-12 text-center mt-20">
                <img src="{$baseUrl}assets/images/extras/franja-azul.png" class="img-responsive" alt="imagen" style="display: initial;width: 60%;" />
            </div>

            <div class="col-md-12 text-center mt-60 no-padding-xs">
                <img src="{$baseUrl}assets/images/extras/somosempresab.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>

            <div class="col-md-offset-2 col-md-8 text-center mt-40">
                <h4 class="h4-empresa">Las Empresas B son aquellas que miden su impacto social y ambiental y se comprometen de forma personal, institucional y legal a tomar decisiones considerando las consecuencias de sus acciones a largo plazo en la comunidad y el medioambiente.</h4>
            </div>

            <div class="col-md-offset-1 col-md-10 text-center mt-60">
                <div class="col-md-3">
                    <img src="{$baseUrl}assets/images/extras/logo-BFTW.png" class="d-inline-block img-responsive img-certifica" alt="imagen" />
                    <img src="{$baseUrl}assets/images/extras/logoempresab.png" class="d-inline-block img-responsive img-certifica" alt="imagen" />
                </div>
                <div class="col-md-9 text-left mt-20">
                    <h4 class="h4-certificada">¡Estamos orgullosas de ser una <span style="color: #fff;">Empresa B</span> certificada, en cada proyecto generamos un triple impacto positivo: económico, social y ambiental!</h4>    
                </div>                
            </div>
        </div>
    </div>
</section>

<section class="section-form-portada">>
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <div id="myCarouselPortada" class="carousel slide" data-ride="carousel" data-interval="false">
                {if !empty($c_superior)}
                    <ol class="carousel-indicators">
                        {foreach $c_superior as $cs}
                            {if $cs['item'] == 0}
                                <li data-target="#myCarouselPortada" data-slide-to="{$cs['item']}" class="active"></li>
                            {else}
                                <li data-target="#myCarouselPortada" data-slide-to="{$cs['item']}"></li>
                            {/if}
                        {/foreach}
                    </ol>                    
                {/if}
                <div class="carousel-inner">
                    {if !empty($c_superior)}
                        {foreach $c_superior as $cs}
                            {if $cs['item'] == 0}
                                <div class="item active">
                                    <div class="img-portada-home">
                                        <img src="{$baseUrl}assets/images/carrusel/{$cs['imagen']}" class="img-responsive img-port" alt="{$cs['imagen']}" />
                                    </div>                        
                                </div>
                            {else}
                                <div class="item">
                                    <div class="img-portada-home">
                                        <img src="{$baseUrl}assets/images/carrusel/{$cs['imagen']}" class="img-responsive img-port" alt="{$cs['imagen']}" />
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>

        <div class="col-md-offset-1 col-md-10 text-center mt-20">
            <span style="color: #fff;font-size: 22px;">Saber más sobre la Certificación B: <br>
                <a href="http://www.sistemab.org" target="_blank" style="color:#00C29A;">http://www.sistemab.org</a>
            </span>
        </div>
    </div>
</section>


<script>    
    (function() {        
        var v1 = document.getElementsByClassName("reproductor-youtube-2");
        for (var n = 0; n < v1.length; n++) {
            var p = document.createElement("div");
            p.innerHTML = labnolThumb2(v1[n].dataset.id);
            p.onclick = labnolIframe2;
            v1[n].appendChild(p);
        }
    })();
    function labnolThumb2(id) {
        return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
    }

    function labnolIframe2() {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=1");
        iframe.setAttribute("class", "embed-responsive-item");
        iframe.setAttribute("width", "485px");
        iframe.setAttribute("height", "250px");
        iframe.setAttribute("frameborder", "1");
        iframe.setAttribute("id", "youtube-iframe");
        this.parentNode.replaceChild(iframe, this);
    }
    function loadPopupVideo(){

    }
</script>