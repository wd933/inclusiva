<section class="section-nuestra-chamba">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <img src="{$baseUrl}assets/images/extras/nuestrachamba-final.png" class="img-responsive img-nuestra-chamba" alt="imagen" />
        </div>
        <div class="col-md-offset-1 col-md-10">
            <div id="myCarouselVideo" class="carousel slide" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    {if !empty($c_chamba)}
                        {foreach $c_chamba as $cc}
                            {if $cc['item'] == 0}
                                <li data-target="#myCarouselVideo" data-slide-to="{$cc['item']}" class="active"></li>
                            {else}
                                <li data-target="#myCarouselVideo" data-slide-to="{$cc['item']}"></li>
                            {/if}
                        {/foreach}
                    {/if}
                </ol>
                <div class="carousel-inner">
                    {if !empty($c_chamba)}
                        {foreach $c_chamba as $cc}
                            {if $cc['item'] == 0}
                                <div class="item active">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive" alt="{$cc['imagen']}" />
                                </div>
                            {else}
                                <div class="item">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cc['imagen']}" class="img-responsive" alt="{$cc['imagen']}" />
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>        
    </div>
</section>

<section class="section-internal">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <div class="col-xs-6 col-md-6 no-padding">
                <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_1['valor']}" class="img-responsive" alt="{$imagen_chamba_1['valor']}" style="display: initial;" />
            </div>
            <div class="col-xs-6 col-md-6 no-padding">
                <div class="col-md-12">
                    <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_2['valor']}" class="img-responsive" alt="{$imagen_chamba_2['valor']}" style="display: initial;" />    
                </div>
                <div class="col-md-12 mt-18">
                    <img src="{$baseUrl}assets/images/extras/{$imagen_chamba_3['valor']}" class="img-responsive img-chamba-3" alt="{$imagen_chamba_3['valor']}" style="display: initial;" />    
                </div>                
            </div>
        </div>
    </div>
</section>

<section class="section-internal">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <img src="{$baseUrl}assets/images/extras/quehacemos.png" class="img-responsive" alt="imagen" style="display: initial;width: 90%;" />
        </div>
    </div>
</section>


<section class="section-internal">
    <div class="container">
        <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-50-xs no-padding-xs">
            <ul id="filters" class="clearfix tplanes">
                <li><span class="filter active" data-filter=".insght"><img src="{$baseUrl}assets/images/extras/iconoinsght-2.png" class="img-responsive" alt="imagen" /></span></li>
                <li><span class="filter" data-filter=".innovacion"><img src="{$baseUrl}assets/images/extras/iconoinnovacion.png" class="img-responsive" alt="imagen" /></span></li>
                <li><span class="filter" data-filter=".impacto"><img src="{$baseUrl}assets/images/extras/iconoimpacto.png" class="img-responsive" alt="imagen" /></span></li>
                <li><span class="filter" data-filter=".training"><img src="{$baseUrl}assets/images/extras/iconotraining.png" class="img-responsive" alt="imagen" /></span></li>
            </ul> 
        </div>

        <div id="portfoliolist">
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-40 portfolio insght" id="insght">
                <img src="{$baseUrl}assets/images/extras/insights.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-20 portfolio insght" id="insght">
                <h4 class="title-insght">Generamos un profundo </h4> 
                <h4 class="title-insght">conocimiento sobre </h4> 
                <h4 class="title-insght">La Base de la Pirámide</h4>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 mt-60 portfolio insght" id="insght">
                <div class="col-md-offset-1 col-md-5">
                    <h5 class="subtitle-insght text-center-mob">Conexión Base</h5>
                    <p class="text-center-mob">
                        Estudios para comprender hábitos y comportamiento en la BdP.
                    </p>
                </div>
                <div class="col-md-5">
                    <h5 class="subtitle-insght text-center-mob">Day in The Life</h5>
                    <p class="text-center-mob">
                        Estudios etnográficos del día a día de la BdP (presencial y/o digital).
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-1 col-md-5 mt-60">
                    <h5 class="subtitle-insght text-center-mob">Data Análitica</h5>
                    <p class="text-center-mob">
                        Estudios cuantitativos sobre perfiles, tendencias y validaciones en la BdP.
                    </p>
                </div>
                <div class="col-md-5 mt-60">
                    <h5 class="subtitle-insght text-center-mob">Asesorías insights BdP</h5>
                    <p class="text-center-mob">
                        Asesorías de conocimiento Base.
                    </p>
                </div>
            </div>

            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-40 portfolio innovacion" id="innovacion">
                <img src="{$baseUrl}assets/images/extras/innovacion.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-20 portfolio innovacion" id="innovacion">
                <h4 class="title-innovacion">Co-creamos soluciones innovadoras</h4> 
                <h4 class="title-innovacion">y disruptivas centradas en la BdP </h4> 
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 mt-60 portfolio innovacion" id="innovacion">
                <div class="col-md-offset-1 col-md-5">
                    <h5 class="subtitle-innovacion">Modelos de Negocios Base</h5>
                    <p>
                        Diseño de productos, servicios y experiencias Base para consumidores.
                    </p>
                </div>
                <div class="col-md-5">
                    <h5 class="subtitle-innovacion">Programas de Fidelización</h5>
                    <p>
                        Programas de fidelización centrado en colaboradores y trabajadores BdP.
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-1 col-md-5 mt-60">
                    <h5 class="subtitle-innovacion">Canales de Distribución</h5>
                    <p>
                        Nuevos canales populares para distribuidores BdP.
                    </p>
                </div>
                <div class="col-md-5 mt-60">
                    <h5 class="subtitle-innovacion">Cadenas Productivas</h5>
                    <p>
                        Nuevas cadenas productivas inclusivas para proveedores de la BdP.
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-3 col-md-6 mt-60">
                    <h5 class="subtitle-innovacion">Programas Sociales</h5>
                    <p>
                        Programas sociales para áreas de influencia BdP.
                    </p>
                </div>
            </div>

            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-40 portfolio impacto" id="impacto">
                <img src="{$baseUrl}assets/images/extras/impacto.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-20 portfolio impacto" id="impacto">
                <h4 class="title-impacto">Acompañamos a las organizaciones</h4> 
                <h4 class="title-impacto">a medir el impacto generado; tanto </h4> 
                <h4 class="title-impacto">para el negocio, cómo para la BdP </h4> 
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 mt-60 portfolio impacto" id="impacto">
                <div class="col-md-offset-1 col-md-5">
                    <h5 class="subtitle-impacto">Diseño Indicadores</h5>
                    <p>
                        Diseño de indicadores de impacto.
                    </p>
                </div>
                <div class="col-md-5">
                    <h5 class="subtitle-impacto">Medición on Going</h5>
                    <p>
                        Medición ágil de proyectos on going.
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-1 col-md-5 mt-60">
                    <h5 class="subtitle-impacto">Doble Impacto</h5>
                    <p>
                        Evaluación de impacto de negocio (rentabilidad / prosperidad).
                    </p>
                </div>
                <div class="col-md-5 mt-60">
                    <h5 class="subtitle-impacto">Sistematización</h5>
                    <p>
                        Sistematización de experiencias.
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-3 col-md-6 mt-60">
                    <h5 class="subtitle-impacto">Evaluaciones</h5>
                    <p>
                        Evaluaciones de proyectos o programas sociales.
                    </p>
                </div>
            </div>


            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-40 portfolio training" id="training">
                <img src="{$baseUrl}assets/images/extras/training.png" class="img-responsive" alt="imagen" style="display: initial;" />
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 text-center mt-20 portfolio training" id="training">
                <h4 class="title-training">Entrenamos a los equipos líderes</h4> 
                <h4 class="title-training">en conocimiento Base </h4>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-offset-1 col-md-10 mt-60 portfolio training" id="training">
                <div class="col-md-offset-1 col-md-5">
                    <h5 class="subtitle-training">¿Quién es la BdP?</h5>
                    <p>
                        Dimensionando a la Base de la Pirámide (1h).
                    </p>
                </div>
                <div class="col-md-5">
                    <h5 class="subtitle-training">PPB</h5>
                    <p>
                        Principios de Pensamiento Base (2h/4h/8h).
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-1 col-md-5 mt-60">
                    <h5 class="subtitle-training">Casos de éxito</h5>
                    <p>
                        Casos de negocios exitosos en la BdP (2h).
                    </p>
                </div>
                <div class="col-md-5 mt-60">
                    <h5 class="subtitle-training">Inmersión <span style="font-family: Arial;font-weight: bold;">360°</span></h5>
                    <p>
                        Inmersión + talleres teóricos (8h/16h).
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-offset-3 col-md-6 mt-60">
                    <h5 class="subtitle-training">Negocios Inclusivos</h5>
                    <p>
                        Negocios inclusivos (2h/8h).
                    </p>
                </div>
            </div>



        </div>
    </div>
</section>



<script>    
    (function() {        
        var v1 = document.getElementsByClassName("reproductor-youtube-2");
        for (var n = 0; n < v1.length; n++) {
            var p = document.createElement("div");
            p.innerHTML = labnolThumb2(v1[n].dataset.id);
            p.onclick = labnolIframe2;
            v1[n].appendChild(p);
        }
    })();
    function labnolThumb2(id) {
        return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
    }

    function labnolIframe2() {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=1");
        iframe.setAttribute("class", "embed-responsive-item");
        iframe.setAttribute("width", "485px");
        iframe.setAttribute("height", "250px");
        iframe.setAttribute("frameborder", "1");
        iframe.setAttribute("id", "youtube-iframe");
        this.parentNode.replaceChild(iframe, this);
    }
    function loadPopupVideo(){

    }
</script>