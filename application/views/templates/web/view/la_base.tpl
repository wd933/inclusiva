<section class="section-la-base">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center">
            <img src="{$baseUrl}assets/images/extras/la-base.png" class="img-responsive img-labase" alt="imagen" />
        </div>    
    </div>
</section>

<section class="">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding mt-40">
            <img src="{$baseUrl}assets/images/extras/queeslabase.png" class="img-responsive" alt="imagen" />
        </div>
    </div>
</section>

<section class="">
    <div class="container-fluid no-padding">
        <div class="col-md-offset-1 col-md-10 mt-40">
            <div class="col-xs-6 col-md-offset-1 col-md-5">
                <img src="{$baseUrl}assets/images/extras/marcofotos-1.png" class="img-responsive" alt="imagen" style="display: initial;" />  
            </div>
            <div class="col-xs-6 col-md-5">
                <img src="{$baseUrl}assets/images/extras/marcofotos-2.png" class="img-responsive" alt="imagen" style="display: initial;" />  
            </div>
            <div class="clearfix"></div>
            <div class="col-md-11 text-right">
                <p class="font-labase"><b>(Fuentes: APEIM 2020, ENAHO 2019, CENSO 2017)</b></p>
            </div>
        </div>
    </div>
</section>

{*
<section class="" style="background: #fff;">
    <div class="container">
        <div class="col-md-offset-2 col-md-8 text-center mt-40">
            <img src="{$baseUrl}assets/images/extras/comohacambiadolabse.png" class="img-responsive img-cambiobase" alt="imagen" />
        </div>
    </div>
</section>

<section class="section-inclusiva" style="background: #fff;">
    <div class="container">
        <div class="col-md-offset-3 col-md-7 mt-20">
            <ul class="ul-la-base">
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> Disminución de los pobres y vulnerables (clase D y E)</li>
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> Aumento importante de los emergentes (clase C)</li>
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> La clásica pirámide comienza tomar forma de rombo</li>
            </ul>
        </div>

        <div class="col-md-offset-2 col-md-8 text-center">
            <img src="{$baseUrl}assets/images/extras/metricabase1.png" class="img-responsive" alt="imagen" />
        </div>

        <div class="col-md-offset-2 col-md-8 text-center mt-40">
            <img src="{$baseUrl}assets/images/extras/enlima.png" class="img-responsive img-lima" alt="imagen" style="max-width: 300px;display: initial;" />
        </div>

        <div class="col-md-offset-3 col-md-7 mt-20">
            <ul class="ul-la-base">
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> Millones de familias tuvieron un importante crecimiento económico, aumentando su capacidad de consumo.</li>
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> La clase C para ser mayoritaria ¡Gran hito!</li>
                <li><img src="{$baseUrl}assets/images/extras/check-azul.png"> Las empresas que apostaron hace 10 años en la Base, hoy se benefician: Ej. Microfinanzas</li>
            </ul>
        </div>


        <div class="col-md-offset-2 col-md-8 text-center">
            <img src="{$baseUrl}assets/images/extras/metricabase2.png" class="img-responsive" alt="imagen" />
        </div>
    </div>
</section>
*}

<section class="">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding mt-20">
            <img src="{$baseUrl}assets/images/extras/muevelaeconomia-2.png" class="img-responsive" alt="imagen" />
        </div>
    </div>
</section>


<section class="section-inclusiva">
    <div class="container-fluid no-padding">
        <div class="col-md-12 no-padding carousel-base">
            <div id="myCarouselCliente" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    {if !empty($c_base)}
                        {foreach $c_base as $cb}
                            {if $cb['item'] == 0}
                                <div class="item active">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cb['imagen']}" class="img-responsive img-full" alt="{$cb['imagen']}" />
                                </div>
                            {else}
                                <div class="item">
                                    <img src="{$baseUrl}assets/images/carrusel/{$cb['imagen']}" class="img-responsive img-full" alt="{$cb['imagen']}" />
                                </div>
                            {/if}
                        {/foreach}
                    {/if}                    
                </div>
                <a class="left carousel-control" href="#myCarouselCliente" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarouselCliente" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="col-md-offset-2 col-md-8 text-center mt-50 mb-50">
        <img src="{$baseUrl}assets/images/extras/soloeslider.png" class="img-responsive" alt="imagen" />
    </div>
</section>
