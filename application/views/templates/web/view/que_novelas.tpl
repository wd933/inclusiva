<section class="section-inclusiva">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center mt-40 mb-30">
            <img src="{$baseUrl}assets/images/extras/boletin-2.png" class="img-responsive" alt="imagen" style="display: initial;width: 90%;" />
        </div>
        <div class="col-md-12 text-center" style="color:#fff">
            Los boletines son publicaciones que buscan compartir aprendizajes y oportunidades en la Base de la Pirámide.
        </div>
    </div>
</section>


<section class="section-boletin">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 text-center mt-40">
            {if !empty($c_boletines)}
                {foreach $c_boletines as $bo}
                    <div class="col-xs-6 col-md-6 mb-30">
                        <div class="">
                            <a href="{$baseUrl}assets/pdf/{$bo['pdf']}" target="_blank">
                                <img src="{$baseUrl}assets/images/boletines/{$bo['imagen']}" class="img-responsive" alt="{$bo['imagen']}" />    
                            </a>                            
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</section>


<section class="section-inclusiva">
    <div class="container">
        <div class="col-md-12 text-center mt-60 mb-20">
            <img src="{$baseUrl}assets/images/extras/inscribete.png" class="img-responsive img-nuestro-equipo" alt="imagen" />
        </div>

        <div class="col-md-offset-3 col-md-6 text-center mb-50" style="color:#fff">
            No te pierdas las próximas ediciones y déjanos tus datos aquí para recibirlas directamente en tu correo.
        </div>

        <div class="col-md-offset-4 col-md-4">
            <form class="form-horizontal form" action="{$baseUrl}registro/accion_suscriptor" autocomplete="off" method="POST">
                <span class="response"></span>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="nombre" style="text-align: left;color: #FFFC00;">Nombres:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="apellidos" style="text-align: left;color: #FFFC00;">Apellidos:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="apellidos" name="apellido" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="correo" style="text-align: left;color: #FFFC00;">Correo:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="correo" name="correo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="trabajo" style="text-align: left;color: #FFFC00;">Lugar de trabajo:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="trabajo" name="lugar_trabajo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="cargo" style="text-align: left;color: #FFFC00;">Cargo:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="cargo" name="cargo" />
                        </div>
                    </div>
                </div>


                <div class="col-md-12 text-center mt-30">
                    <button class="btn btn-primary btn-contact save" title="Enviar"> Enviar</button>
                </div>                
            </form>
        </div>
    </div>
</section>

<section class="banner-video-2">
    <div class="container">
        <div class="col-xs-12 col-xs-offset-0 col-md-offset-2 col-md-8">
            <div class="contenedor-youtube video-inclusive">
                <div class="embed-responsive embed-responsive-16by9" id="section_1_2">
                    <div class="contenedor-youtube">
                        <div class="reproductor-youtube reproductor-youtube-2" data-id="{$video_inclusiva}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-inclusiva">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-center mt-30 mb-30">
            <img src="{$baseUrl}assets/images/extras/noticias.png" class="img-responsive img-nuestro-equipo" alt="imagen" />
        
        </div>

        <div class="col-md-10 col-md-offset-1 mt-10">
            {if !empty($c_noticias)}
                {foreach $c_noticias as $not}
                    <div class="col-md-6 col-sm-6 mb-60">
                        <div class="card-noticia">
                            <div class="card-not-img">
                                <img src="{$baseUrl}assets/images/noticias/{$not['imagen']}" class="img-responsive" alt="{$not['imagen']}" />
                            </div>
                            <div class="card-not-body">
                                <h3>{$not['titulo']}</h3>
                                <p>{$not['subtitular']}</p>

                                <a href="{$not['url']}" target="_blank"><i class="fa fa-angle-right"></i> Leer más</a>
                            </div>
                        </div>
                    </div>
                {/foreach}
            {/if}

        </div>

        
    </div>
</section>


<script>    
    (function() {        
        var v1 = document.getElementsByClassName("reproductor-youtube-2");
        for (var n = 0; n < v1.length; n++) {
            var p = document.createElement("div");
            p.innerHTML = labnolThumb2(v1[n].dataset.id);
            p.onclick = labnolIframe2;
            v1[n].appendChild(p);
        }
    })();
    function labnolThumb2(id) {
        return '<img class="imagen-previa" src="//i.ytimg.com/vi/' + id + '/hqdefault.jpg"><div class="youtube-play"></div>';
    }

    function labnolIframe2() {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=1");
        iframe.setAttribute("class", "embed-responsive-item");
        iframe.setAttribute("width", "485px");
        iframe.setAttribute("height", "250px");
        iframe.setAttribute("frameborder", "1");
        iframe.setAttribute("id", "youtube-iframe");
        this.parentNode.replaceChild(iframe, this);
    }
    function loadPopupVideo(){

    }
</script>