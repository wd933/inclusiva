<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <style>
        body{
            font-family: "Arial";
        }
    </style>
    <body>
        <table width="100%" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>            
                    <td>                
                        <table align="center" bgcolor="#f5f5f5">
                            <tbody>
                                <tr>
                                    <td width="640">
                                        <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr style="text-align: center;">
                                                    <td style="padding: 15px 5px 0px 0px;border-bottom: 1px solid #ccc">
                                                        <img src="{$logo}" width="200">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2" style="padding:20px" align="left">

                                                        <font>Estimado(a).</font>
                                                        <br><br>
                                                        Se ha registrado una solicitud de contacto y los datos son los siguientes:
                                                        <br><br>
                                                        <b>Nombres y Apellidos:</b> {$nombre}<br>
                                                        <b>Telefono:</b> {$telefono}<br>
                                                        <b>Email:</b> {$email}<br>
                                                        <b>Fecha de registro:</b> {$fecha}<br>
                                                        <b>Mensaje:</b> <br>
                                                            {$mensaje|nl2br}
                
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>