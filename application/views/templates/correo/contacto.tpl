<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <style>
        body{
            font-family: "Arial";
        }
    </style>
    <body>
        <table width="100%" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>            
                    <td>                
                        <table align="center" bgcolor="#f5f5f5">
                            <tbody>
                                <tr>
                                    <td width="640">
                                        <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr style="text-align: center;">
                                                    <td style="padding: 15px 5px 0px 0px;border-bottom: 1px solid #ccc">
                                                        <img src="http://sistemas-dentales.com/webalemana/assets/img/logo.png" width="200">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table width="100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2" style="padding:20px" align="left">

                                                        <font>Estimado(a).</font>
                                                        <br><br>
                                                        Se ha registrado una solicitud para cita y los datos son los siguientes:
                                                        <br><br>
                                                        Nombres y Apellidos: {$nombre}<br>
                                                        Email: {$email}<br>
                                                        Celular: {$celular}<br>
                                                        Especialidad: {$especialidad}<br>
                                                        Fecha para la cita: {$fecha}<br>
                                                        Comentario: <br>
                                                            {$comentario|nl2br}
                
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>