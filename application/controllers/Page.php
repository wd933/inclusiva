<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Page extends CI_Controller {

    private $items = array();

    public function __construct() {
        parent::__construct();
        /*
         * Configuración para librerias, helpers y modelos
         */
        $library = array('parser', 'pagination', 'alerta', 'archivo', 'url_comp', 'session');
        $helper = array('url', 'base64_url');
        $model = array('m_cliente', 'm_socio', 'm_carrusel_cliente', 'm_carrusel_superior', 'm_video', 'm_carrusel_chamba', 'm_carrusel_base', 'm_noticia', 'm_boletin', 'm_equipo');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);

        /*
         * Configuración personalizada
         */
        $proyecto = $this->m_configuracion->mostrar(array('c.campo'=>'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['_favicon'] = $favicon['valor'];
        $logo = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));
        $this->items['_logo'] = $logo['valor'];
        $correo = $this->m_configuracion->mostrar(array('c.campo'=>'correo'));
        $this->items['_correo'] = $correo['valor'];

        /* redes sociales*/
        $facebook = $this->m_configuracion->mostrar(array('c.campo'=>'facebook'));
        $this->items['_facebook'] = $facebook['valor'];
        $instagram = $this->m_configuracion->mostrar(array('c.campo'=>'instagram'));
        $this->items['_instagram'] = $instagram['valor'];
        $linkedin = $this->m_configuracion->mostrar(array('c.campo'=>'linkedin'));
        $this->items['_linkedin'] = $linkedin['valor'];
        $twitter = $this->m_configuracion->mostrar(array('c.campo'=>'twitter'));
        $this->items['_twitter'] = $twitter['valor'];

        $this->items['baseUrl'] = base_url();
        $this->items['time'] = time();
    }
    
    
    public function home() {
        $data['title_page'] = $this->items['proyecto'];
        $data['description_page'] = '';
        $data['key_page'] = '';

        $c_superior = $this->m_carrusel_superior->mostrar_activos(FALSE, FALSE, ["cs.posicion"=>"asc"]);
        if(!empty($c_superior)){
            $i = 0;
            foreach ($c_superior as $key => $value) {
                $c_superior[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_superior'] = $c_superior;


        $c_clientes = $this->m_carrusel_cliente->mostrar_activos(FALSE, FALSE, ["cc.posicion"=>"asc"]);
        if(!empty($c_clientes)){
            $i = 0;
            foreach ($c_clientes as $key => $value) {
                $c_clientes[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_clientes'] = $c_clientes;


        $c_videos = $this->m_video->mostrar_activos(FALSE, FALSE, ["v.posicion"=>"asc"]);
        if(!empty($c_videos)){
            $i = 0;
            foreach ($c_videos as $key => $value) {
                $c_videos[$key]['item'] = $i;
                $c_videos[$key]['codigo'] = $this->archivo->youtube($value['url']);
                $i++;
            }
        }
        $data['c_videos'] = $c_videos;

        $c_noticias = $this->m_noticia->mostrar_activos(FALSE, FALSE, ["n.fecha_registro"=>"asc"]);
        if(!empty($c_noticias)){
            $i = 0;
            foreach ($c_noticias as $key => $value) {
                $c_noticias[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_noticias'] = $c_noticias;
        
        
        $data = array_merge($data, $this->items);
        $this->template->web("home", $data);
    }


    public function inclusiva() {
        $data['title_page'] = $this->items['proyecto'] . ' | Inclusiva';
        $data['description_page'] = '';
        $data['key_page'] = '';

        $tmpVideo = $this->m_configuracion->mostrar(array('c.campo'=>'video_inclusiva'));
        $data['video_inclusiva'] = $this->archivo->youtube($tmpVideo['valor']);

        $c_superior = $this->m_carrusel_superior->mostrar_activos(FALSE, FALSE, ["cs.posicion"=>"asc"]);
        if(!empty($c_superior)){
            $i = 0;
            foreach ($c_superior as $key => $value) {
                $c_superior[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_superior'] = $c_superior;

        $c_equipo = $this->m_equipo->mostrar_activos(FALSE, FALSE, ["e.posicion"=>"asc"]);
        if(!empty($c_equipo)){
            $i = 0;
            foreach ($c_equipo as $key => $value) {
                $c_equipo[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_equipo'] = $c_equipo;

        $data = array_merge($data, $this->items);
        $this->template->web("inclusiva", $data);
    }

    
    public function nuestra_chamba() {
        $data['title_page'] = $this->items['proyecto'] . ' | Nuestra Chamba';
        $data['description_page'] = '';
        $data['key_page'] = '';

        $c_chamba = $this->m_carrusel_chamba->mostrar_activos(FALSE, FALSE, ["cc.posicion"=>"asc"]);
        if(!empty($c_chamba)){
            $i = 0;
            foreach ($c_chamba as $key => $value) {
                $c_chamba[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_chamba'] = $c_chamba;

        $data['imagen_chamba_1'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_1'));
        $data['imagen_chamba_2'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_2'));
        $data['imagen_chamba_3'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_3'));

        $data = array_merge($data, $this->items);
        $this->template->web("nuestra_chamba", $data);
    }


    public function resultados_power() {
        $data['title_page'] = $this->items['proyecto'] . ' | Resultados Power';
        $data['description_page'] = '';
        $data['key_page'] = '';

        $data = array_merge($data, $this->items);
        $this->template->web("resultados_power", $data);
    }


    public function caseritos() {
        $data['title_page'] = $this->items['proyecto'] . ' | Caseritos';
        $data['description_page'] = '';
        $data['key_page'] = '';


        $clientes = $this->m_cliente->mostrar_activos(FALSE, FALSE, ["c.posicion"=>"asc"]);
        if(!empty($clientes)){
            $i = 1;
            foreach ($clientes as $key => $value) {
                $clientes[$key]['item'] = $i;
                $i++;
            }
        }
        $data['clientes'] = $clientes;


        $socios = $this->m_socio->mostrar_activos(FALSE, FALSE, ["s.posicion"=>"asc"]);
        if(!empty($socios)){
            $i = 1;
            foreach ($socios as $key => $value) {
                $socios[$key]['item'] = $i;
                $i++;
            }
        }
        $data['socios'] = $socios;

        $c_clientes = $this->m_carrusel_cliente->mostrar_activos(FALSE, FALSE, ["cc.posicion"=>"asc"]);
        if(!empty($c_clientes)){
            $i = 0;
            foreach ($c_clientes as $key => $value) {
                $c_clientes[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_clientes'] = $c_clientes;

        $data = array_merge($data, $this->items);
        $this->template->web("caseritos", $data);
    }
    

    public function la_base_what() {
        $data['title_page'] = $this->items['proyecto'] . ' | La Base';
        $data['description_page'] = '';
        $data['key_page'] = '';

        $c_base = $this->m_carrusel_base->mostrar_activos(FALSE, FALSE, ["cb.posicion"=>"asc"]);
        if(!empty($c_base)){
            $i = 0;
            foreach ($c_base as $key => $value) {
                $c_base[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_base'] = $c_base;

        $data = array_merge($data, $this->items);
        $this->template->web("la_base", $data);
    }


    public function que_novelas() {
        $data['title_page'] = $this->items['proyecto'] . ' | Que novelas';
        $data['description_page'] = '';
        $data['key_page'] = '';

        $tmpVideo = $this->m_configuracion->mostrar(array('c.campo'=>'video_inclusiva'));
        $data['video_inclusiva'] = $this->archivo->youtube($tmpVideo['valor']);

        $c_boletines = $this->m_boletin->mostrar_activos(FALSE, FALSE, ["b.fecha_registro"=>"asc"]);
        if(!empty($c_boletines)){
            $i = 0;
            foreach ($c_boletines as $key => $value) {
                $c_boletines[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_boletines'] = $c_boletines;


        $c_noticias = $this->m_noticia->mostrar_activos(FALSE, FALSE, ["n.fecha_registro"=>"asc"]);
        if(!empty($c_noticias)){
            $i = 0;
            foreach ($c_noticias as $key => $value) {
                $c_noticias[$key]['item'] = $i;
                $i++;
            }
        }
        $data['c_noticias'] = $c_noticias;


        $data = array_merge($data, $this->items);
        $this->template->web("que_novelas", $data);
    }
}

