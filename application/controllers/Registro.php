<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Registro extends CI_Controller {

    private $items = array();

    public function __construct() {
        parent::__construct();
        
        $library = ['parser', 'pagination', 'alerta', 'archivo', 'url_comp', 'session', 'php_mailer'];
        $helper = ['url', 'base64_url'];
        $model = ['m_formulario_web', 'm_suscriptor'];
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);

        $proyecto = $this->m_configuracion->mostrar(array('c.campo'=>'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['_favicon'] = $favicon['valor'];
        $this->_correo = $this->m_configuracion->mostrar(array('c.campo'=>'correo'));


        $this->items['baseUrl'] = base_url();
        $this->items['time'] = time();
    }
    
    
    public function index() { }


    public function accion_contacto() {
        $nombre = $this->input->post('nombre');
        $telefono = $this->input->post('telefono');
        $email = $this->input->post('email');        
        $mensaje = $this->input->post('mensaje');

        //VALIDACION DE CAMPOS
        $error = '';
        $error .= $this->mantenimiento->validacion($nombre, 'required|alphaspecial', 'Nombres y Apellidos');
        $error .= $this->mantenimiento->validacion($telefono, 'required', 'Telefono');
        $error .= $this->mantenimiento->validacion($email, 'required|email', 'Email');        
        $error .= $this->mantenimiento->validacion($mensaje, 'required', 'Mensaje');

        if ($error != '') {
            $response = ['tipo' => '300', 'alert' => $this->alerta->swal_error($error, TRUE)];
            echo json_encode($response); exit;
        }
        
        $datos['nombre'] = strip_tags($nombre);
        $datos['telefono'] = strip_tags($telefono);
        $datos['email'] = strip_tags($email);        
        $datos['mensaje'] = strip_tags($mensaje);
        $datos['fecha_registro'] = date("Y-m-d H:i:s");
        $datos['ip'] = $this->input->ip_address();
        $result = $this->m_formulario_web->insertar($datos);
        
        if($result){
            // envio de correo de respuesta
            $data = [];
            $data['logo'] = base_url().'assets/images/logo-inclusiva.png';
            $data['nombre'] = $nombre;
            $data['email'] = $email;
            $data['telefono'] = $telefono;
            $data['mensaje'] = $mensaje;
            $data['fecha'] = date('d/m/Y');
            $html = $this->smarty_tpl->view('correo/correo_contacto', $data, TRUE);

            $from = "no-responder@inclusiva.com.pe";
            $fromName = "Aviso Página web Inclusiva";

            $this->php_mailer->isSendmail();
            $this->php_mailer->Subject = 'Registro de contacto web';
            $this->php_mailer->addReplyTo($from, $fromName);
            $this->php_mailer->setFrom($from, $fromName);
            //$this->php_mailer->addAddress('christianqe1393@gmail.com', 'Contacto web');
            $this->php_mailer->addAddress($this->_correo['valor'], 'Contacto web');
            $this->php_mailer->msgHTML($html);
            $this->php_mailer->send();                

            
            $response = [
                'tipo' => '200',
                'alert' => $this->alerta->swal_success("Se registro correctamente, en breve nos estaremos comunicando con usted."),
                'redirect' => $this->url_comp->actualizar_tiempo('3000')
            ];
            echo json_encode($response); exit;

        }else{
            $error = 'No se puede procesar su solicitud, intentelo luego...';
            $response = ['tipo' => '300', 'alert' => $this->alerta->swal_error($error, TRUE)];
            echo json_encode($response); exit;
        }
    }


    public function accion_suscriptor() {
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $correo = $this->input->post('correo');
        $lugar_trabajo = $this->input->post('lugar_trabajo');
        $cargo = $this->input->post('cargo');

        //VALIDACION DE CAMPOS
        $error = '';
        $error .= $this->mantenimiento->validacion($nombre, 'required|alphaspecial', 'Nombres');
        $error .= $this->mantenimiento->validacion($apellido, 'required|alphaspecial', 'Apellidos');
        $error .= $this->mantenimiento->validacion($correo, 'required', 'correo');

        if ($error != '') {
            $response = ['tipo' => '300', 'alert' => $this->alerta->swal_error($error, TRUE)];
            echo json_encode($response); exit;
        }elseif ($this->m_suscriptor->existe_campo('correo', $correo)) {
            $msg = 'El correo ya se encuentra registrado';
            $response = ['tipo' => '300', 'alert' => $this->alerta->swal_error($msg, TRUE)];
            echo json_encode($response); exit;
        }
        
        $datos['nombre'] = strip_tags($nombre);
        $datos['apellido'] = strip_tags($apellido);
        $datos['correo'] = strip_tags($correo);        
        $datos['lugar_trabajo'] = strip_tags($lugar_trabajo);
        $datos['cargo'] = strip_tags($cargo);
        $datos['fecha_registro'] = date("Y-m-d H:i:s");
        $datos['ip'] = $this->input->ip_address();
        $result = $this->m_suscriptor->insertar($datos);
        
        if($result){
            $response = [
                'tipo' => '200',
                'alert' => $this->alerta->swal_success("Se inscribio correctamente."),
                'redirect' => $this->url_comp->actualizar_tiempo('2000')
            ];
            echo json_encode($response); exit;

        }else{
            $error = 'No se puede procesar su solicitud, intentelo luego...';
            $response = ['tipo' => '300', 'alert' => $this->alerta->swal_error($error, TRUE)];
            echo json_encode($response); exit;
        }
    }
    
}

