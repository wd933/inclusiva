<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Suscriptor extends CI_Controller {

    private $_process;
    private $_result;

    public function __construct() {
        parent::__construct();
        
        $library = array('session_manager', 'archivo');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_suscriptor');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Listado de Suscriptores';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_suscriptor->mostrar_activos(FALSE, FALSE, array("s.fecha_registro" => "desc"));        
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {                
                $data['lista'][] = array(
                    'id' => $items['idsuscriptor'],
                    'numero' => $i,
                    'nombre' => $items['nombre'],
                    'apellido' => $items['apellido'],
                    'correo' => $items['correo'],
                    'lugar_trabajo' => $items['lugar_trabajo'],
                    'cargo' => $items['cargo'],
                    'f_registro' => date("d-m-Y g:s a", strtotime($items['fecha_registro'])),
                );
                $i++;
            }
        }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Listado de suscriptores';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_suscriptor", $data);
    }

    
}

