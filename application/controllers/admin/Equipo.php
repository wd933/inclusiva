<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Equipo extends CI_Controller {
    public function __construct() {
        parent::__construct();
        /*
         * DECLARACION DE LIBRERIAS, HELPERS Y MODELOS
         */
        $library = array('session_manager', 'archivo', 'orden');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_equipo');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * CONFIGURACION PERSONAL
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Nuestro equipo';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_equipo->mostrar_activos(FALSE, FALSE, ["e.posicion"=>"asc"]);        
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {
                $accion = $this->mantenimiento->accion($items['idequipo'], 'editar2|subir|bajar|eliminar', 'equipo', $items['oculto']);
                    $data['lista'][] = array(
                        'id' => $items['idequipo'],
                        'numero' => $i,
                        'nombre' => $items['nombre'],
                        'cargo' => $items['cargo'],
                        'imagen' => $items['imagen'],
                        'orden' => $items['posicion'],
                        'accion' => $accion,
                    );
                    $i++;
                }
            }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Nuestro equipo';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_equipo", $data);
    }

    public function agregar(){
        $login = $this->session_manager->datos_usuario_logueado();
        $data = array();

        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view('admin/view/modal_equipo', $data, TRUE);
        $datos['titulo'] = "Agregar miembro del equipo";
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function editar($id = '') {
        $login = $this->session_manager->datos_usuario_logueado();
        if ($id == '') {
            echo $this->url_comp->direccionar(base_url() . 'admin/equipo/listar', TRUE);exit;
        }
        
        $where = array('e.idequipo' => $id, 'e.oculto' => 0);
        $temp = $this->m_equipo->mostrar($where);
        if (!empty($temp)) {
            $data['id'] = $temp['idequipo'];
            $data['nombre'] = $temp['nombre'];
            $data['cargo'] = $temp['cargo'];
            $data['imagen'] = $temp['imagen'];
        } else {
            echo $this->url_comp->direccionar(base_url() . 'admin/equipo/listar', TRUE);exit;
        }
        /* ------------------------------------------------------------ */
        $datos['titulo'] = "Editar miembro del equipo";
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view("admin/view/modal_equipo", $data, TRUE);
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function accion() {
        $id = $this->input->post('id');
        $nombre = $this->input->post('nombre');
        $cargo = $this->input->post('cargo');
        $imagen = $this->archivo->archivo_1('imagen', 'single');
       
        $error = '';
        $error .= $this->mantenimiento->validacion($nombre, 'required', 'Nombre');
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }

        if ($id == '') {
            if ($imagen !== FALSE){
                $mark = array('marca' => '', 'tipo' => 'string');
                $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/equipo', $mark, 1600, $this->items['proyecto']);
            }else{
                echo $this->alerta->swal_error('Debe Elegir una Imagen...', TRUE); exit;
            }

            $datos['nombre'] = $nombre;
            $datos['cargo'] = $cargo;
            $datos['imagen'] = $newImagen;
            $datos['fecha_registro'] = date('Y-m-d H:i:s');
            $datos['idusuario'] = $this->_session->sys_id;
            
            $result = $this->m_equipo->insertar_posicion($datos);
            if($result){
                echo $this->alerta->swal_success('Se registro correctamente...');
                echo $this->url_comp->actualizar_tiempo('1500'); exit;
            }else{
                echo $this->alerta->swal_error('Hubo problemas...', TRUE); exit;
            }            
        }else{
            $where = array('e.idequipo' => $id, 'e.oculto' => 0);
            $tmpDatos = $this->m_equipo->mostrar($where); 
            if (!empty($tmpDatos)) {
                if ($imagen !== FALSE){
                    $mark = array('marca' => '', 'tipo' => 'string');
                    $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/equipo', $mark, 1600, $this->items['proyecto']);
                    $this->archivo->eliminar_imagen($tmpDatos['imagen'], 'assets/images/equipo');
                }else{
                    $newImagen = $tmpDatos['imagen'];
                }                
                $datos['nombre'] = $nombre;                
                $datos['cargo'] = $cargo;
                $datos['imagen'] = $newImagen;
                $datos['fecha_modificacion'] = date('Y-m-d H:i:s');
                $resultSet = $this->m_equipo->actualizar($datos, 'idequipo', $tmpDatos['idequipo']);
                if ($resultSet === TRUE) {
                    echo $this->alerta->swal_success('Se ha editado correctamente...');
                    echo $this->url_comp->direccionar_tiempo(base_url().'admin/equipo/listar','1500');exit;
                }
                
            } else {
                echo $this->url_comp->direccionar(base_url() . 'admin/equipo/listar', TRUE);exit;
            }
        }
    }


    public function subir_posicion() {
        $id = $this->input->post('id');
        $resultSet = $this->m_equipo->exists(array('e.idequipo' => $id, 'e.oculto' => 0));
        if (!empty($resultSet)) {
            $imagen = $this->m_equipo->mostrar(array('e.idequipo' => $id, 'e.oculto' => 0));
            $lista_imagen = $this->m_equipo->mostrar_todo();
            $data = array();
            foreach ($lista_imagen as $items) {
                $data[] = (int) $items['posicion'];
            }
            $result = $this->orden->subir($data, $imagen['posicion']);
            $this->m_equipo->ordenar_posicion($result);
            echo $this->url_comp->actualizar();
            EXIT;
        } else {
            echo $this->alerta->mensaje_error('Hubo problemas', TRUE);
            EXIT;
        }
    }

    public function bajar_posicion() {
        $id = $this->input->post('id');
        $resultSet = $this->m_equipo->exists(array('e.idequipo' => $id, 'e.oculto' => 0));
        if (!empty($resultSet)) {
            $imagen = $this->m_equipo->mostrar(array('e.idequipo' => $id, 'e.oculto' => 0));
            $lista_imagen = $this->m_equipo->mostrar_todo();
            $data = array();
            foreach ($lista_imagen as $items) {
                $data[] = (int) $items['posicion'];
            }
            $result = $this->orden->bajar($data, $imagen['posicion']);
            $this->m_equipo->ordenar_posicion($result);
            echo $this->url_comp->actualizar();
            EXIT;
        } else {
            echo $this->alerta->mensaje_error('Hubo problemas', TRUE);
            EXIT;
        }
    }


    public function accion_eliminar($id = '') {
        if ($id == '') {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/equipo/listar', TRUE); exit;
        }
        $where = array('e.idequipo' => $id, 'e.oculto' => 0);
        $resultSet = $this->m_equipo->exists($where);
        if ($resultSet === FALSE) {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/equipo/listar', TRUE); exit;
        }
        $imagen = $this->m_equipo->mostrar($where);
        $this->archivo->eliminar_imagen($imagen['logo'], 'assets/images/equipo');
        $this->m_equipo->eliminar($id);
        $list = $this->m_equipo->mostrar_todo();
        $result = array();
        for ($i = 0; $i < count($list); $i++) {
            $result[] = $i + 1;
        }
        $this->m_equipo->ordenar_posicion($result);
        echo $this->url_comp->actualizar_tiempo('1200'); exit;
    }


}

