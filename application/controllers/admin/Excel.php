<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Excel extends CI_Controller {

    public function __construct() {
        parent::__construct();

        /*
         * Configuración para librerias, helpers y modelos
         */
        $library = array('session_manager', 'archivo');
        $helper = array('url', 'form');
        $model = array('m_formulario_web', 'm_suscriptor');
        $this->load->library($library);
        $this->load->library('Classes/PHPExcel.php', 'Classes/PHPExcel/Reader/Excel2007.php');
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * Configuración personalizada
        */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $this->items['time'] = time();

    }


    public function exportar_formulario_web() {
        $login = $this->session_manager->datos_usuario_logueado();

        $fi = $this->input->get('fi');
        $ff = $this->input->get('ff');

        $newFi = date("Y-m-d", strtotime($fi));
        $newFf = date("Y-m-d", strtotime($ff));

        $lista = $this->m_formulario_web->select_rango($newFi, $newFf);
        /*$lista = $this->m_formulario_web->mostrar_cuando(array('f.idorigen' => '1', 'f.oculto' => '0'), FALSE, FALSE, array("f.fecha_registro" => "desc")); */

        if(!empty($lista)){
            $x = 1;
            $i = 2;
            $this->phpexcel->getProperties()->setCreator("Arkos Noem Arenom")
                    ->setLastModifiedBy("Arkos Noem Arenom")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");

            foreach ($lista AS $items) {
                // configuramos las propiedades del documento
                // agregamos información a las celdas
                $_especialidad = $this->archivo->get_especialidad($items['idespecialidad']);
                $this->phpexcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', "N°")
                        ->setCellValue('B1', "Nombres y Apellidos")
                        ->setCellValue('C1', "Email")
                        ->setCellValue('D1', "Celular")
                        ->setCellValue('E1', "Especialidad")
                        ->setCellValue('F1', "Fecha de Registro")
                        ->setCellValue('G1', "Fecha para Cita")
                        ->setCellValue('A' . $i, $x)
                        ->setCellValue('B' . $i, $items['nombre'])
                        ->setCellValue('C' . $i, $items['email'])
                        ->setCellValue('D' . $i, $items['celular'])
                        ->setCellValue('E' . $i, $_especialidad)
                        ->setCellValue('F' . $i, date("d-m-Y H:i", strtotime($items['fecha_registro'])))
                        ->setCellValue('G' . $i, date("d-m-Y", strtotime($items['fecha_cita'])));
                $i++;
                $x++;
            }

            $estiloTituloReporte = array(
                'font' => array(
                    'name'      => 'Calibri',
                    'bold'      => true,
                    'italic'    => false,
                    'strike'    => false,
                    'size' =>12,
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
                ),
                'fill' => array(
                  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array(
                        'argb' => '000')
              ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation' => 0,
                    'wrap' => TRUE
                )
            );

            $this->phpexcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($estiloTituloReporte);

            //ancho automatico para las columnas
            for($i = 'A'; $i <= 'G'; $i++){
                $this->phpexcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
            }

            // Renombramos la hoja de trabajo
            $this->phpexcel->getActiveSheet()->setTitle('Formularios Web');

           // redireccionamos la salida al navegador del cliente (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="formulario_web_'.date("d-m-Y").'.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
            $objWriter->save('php://output');
        }else{
            echo "No se encontraron datos para exportar...";
        }
        
    }


    public function exportar_suscriptor() {
        // $fi = $this->input->get('fi');
        // $ff = $this->input->get('ff');
        // $newFi = date("Y-m-d", strtotime($fi));
        // $newFf = date("Y-m-d", strtotime($ff));

        $lista = $this->m_suscriptor->mostrar_activos(false, false, ['s.fecha_registro' => 'desc']);
        //echo "<pre>";print_r($lista);exit;

        if(!empty($lista)){
            $x = 1;
            $i = 2;
            $this->phpexcel->getProperties()->setCreator("Arkos Noem Arenom")
                    ->setLastModifiedBy("Arkos Noem Arenom")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");

            foreach ($lista AS $items) {
                $this->phpexcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', "N°")
                        ->setCellValue('B1', "Nombres")
                        ->setCellValue('C1', "Apellidos")
                        ->setCellValue('D1', "Correo")
                        ->setCellValue('E1', "Lugar de trabajo")
                        ->setCellValue('F1', "Cargo")
                        ->setCellValue('G1', "Fecha registro")
                        ->setCellValue('H1', "Hora registro")
                        ->setCellValue('A' . $i, $x)
                        ->setCellValue('B' . $i, $items['nombre'])
                        ->setCellValue('C' . $i, $items['apellido'])
                        ->setCellValue('D' . $i, $items['correo'])
                        ->setCellValue('E' . $i, $items['lugar_trabajo'])
                        ->setCellValue('F' . $i, $items['cargo'])
                        ->setCellValue('G' . $i, date("d-m-Y", strtotime($items['fecha_registro'])))
                        ->setCellValue('H' . $i, date("g:i a", strtotime($items['fecha_registro'])));
                $i++;
                $x++;
            }

            $estiloTituloReporte = array(
                'font' => array(
                    'name'      => 'Calibri',
                    'bold'      => true,
                    'italic'    => false,
                    'strike'    => false,
                    'size' =>12,
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
                ),
                'fill' => array(
                  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array(
                        'argb' => '000')
              ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation' => 0,
                    'wrap' => TRUE
                )
            );

            $this->phpexcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($estiloTituloReporte);

            //ancho automatico para las columnas
            for($i = 'A'; $i <= 'H'; $i++){
                $this->phpexcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
            }

            // Renombramos la hoja de trabajo
            $this->phpexcel->getActiveSheet()->setTitle('suscriptores');

           // redireccionamos la salida al navegador del cliente (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="suscriptores_'.date("d-m-Y").'.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
            $objWriter->save('php://output');
        }else{
            echo "No se encontraron datos para exportar...";
        }
        
    }



}
