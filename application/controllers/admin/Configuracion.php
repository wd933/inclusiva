<?php
@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Configuracion extends CI_Controller {
    private $_process;
    private $_result;

    public function __construct() {
        parent::__construct();
        /*
         * Configuración para librerias, helpers y modelos
         */
        $library = array('session_manager', 'archivo', 'documento');
        $helper = array();
        $model = array('m_configuracion');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * Configuración personalizada
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        
        
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];

        $this->items['baseUrl'] = base_url();
        $this->items['time'] = time();
    }


    public function panel() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Panel de Configuracion';
        $data['titulo'] = 'Panel de Configuracion';

        // PAGINA
        $data['proyecto_nombre'] = $this->m_configuracion->mostrar(array('c.campo'=>'proyecto_nombre'));
        $data['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));
        $data['favicon'] = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));

        // REDES
        $data['facebook'] = $this->m_configuracion->mostrar(array('c.campo'=>'facebook'));
        $data['youtube'] = $this->m_configuracion->mostrar(array('c.campo'=>'youtube'));
        $data['instagram'] = $this->m_configuracion->mostrar(array('c.campo'=>'instagram'));
        $data['whatsapp'] = $this->m_configuracion->mostrar(array('c.campo'=>'whatsapp'));

        // IMAGEN FOOTER
        $data['footer'] = $this->m_configuracion->mostrar(array('c.campo'=>'fondo_inter_footer'));

        // DATOS DEL CONTACTO
        $data['direccion'] = $this->m_configuracion->mostrar(array('c.campo'=>'direccion'));
        $data['telefono'] = $this->m_configuracion->mostrar(array('c.campo'=>'telefono'));
        $data['correo'] = $this->m_configuracion->mostrar(array('c.campo'=>'correo'));
        $data['horario'] = $this->m_configuracion->mostrar(array('c.campo'=>'horario'));
        $data['googlemap'] = $this->m_configuracion->mostrar(array('c.campo'=>'googlemap'));

        // OTROS DATOS
        $data['video_inclusiva'] = $this->m_configuracion->mostrar(array('c.campo'=>'video_inclusiva'));


        $data['imagen_chamba_1'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_1'));
        $data['imagen_chamba_2'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_2'));
        $data['imagen_chamba_3'] = $this->m_configuracion->mostrar(array('c.campo'=>'imagen_chamba_3'));
        
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("configuracion", $data);
    }


    public function accion_pagina() {
        $logo = $this->archivo->archivo_1('logo', 'single');
        // $favicon = $this->archivo->archivo_1('favicon', 'single');
        $favicon = $this->documento->tipo_documento('favicon', 'single');
        $proyecto = $this->input->post('proyecto');
        //echo "<pre>";print_r($favicon);exit;
        
        $error = '';
        $error .= $this->mantenimiento->validacion($proyecto, 'required', 'Titulo de la página');
        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }

        if ($logo !== FALSE){
            $tmp_logo = $this->m_configuracion->mostrar(array('c.campo' => 'logo'));
            $mark = array('marca' => '', 'tipo' => 'string');
            $n_imagen = $this->archivo->guardar_imagen($logo, 'assets/images/', $mark, 1600, $this->items['proyecto']);
            $this->archivo->eliminar_imagen($tmp_logo['valor'], 'assets/images/');
            $data['valor'] = $n_imagen;
            $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'logo'));
        }

        // if ($favicon !== FALSE){
        //     $tmp_favicon = $this->m_configuracion->mostrar(array('c.campo' => 'favicon'));
        //     $mark = array('marca' => '', 'tipo' => 'string');
        //     $n_imagen = $this->archivo->guardar_imagen($favicon, 'assets/images/', $mark, 1600, $this->items['proyecto']);
        //     $this->archivo->eliminar_imagen($tmp_favicon['valor'], 'assets/images/');
        //     $data['valor'] = $n_imagen;
        //     $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'favicon'));
        // }

        if ($favicon !== FALSE){
            $tmp_favicon = $this->m_configuracion->mostrar(array('c.campo' => 'favicon'));
            $mark = array('marca' => '', 'tipo' => 'string');
            $n_imagen = $this->documento->save_documento($favicon, 'assets/images', 5);
            //echo "<pre>";print_r($n_imagen);exit;
            $this->archivo->eliminar_imagen($tmp_favicon['valor'], 'assets/images');
            $data['valor'] = $n_imagen;
            $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'favicon'));
        }
        
        $tmp_proyecto['valor'] = $proyecto;
        $this->m_configuracion->actualizar($tmp_proyecto, array('campo'=>'proyecto_nombre'));
                   
        echo $this->alerta->swal_success('LOS DATOS SE ACTUALIZARON CORRECTAMENTE...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/configuracion/panel','2000'); exit;
    }


    public function accion_redes_sociales() {
        $facebook = $this->input->post('facebook');
        $instagram = $this->input->post('instagram');
        $whatsapp = $this->input->post('whatsapp');
        $youtube = $this->input->post('youtube');

        $error = '';
        $error .= $this->mantenimiento->validacion($facebook, 'required', 'Facebook');
        $error .= $this->mantenimiento->validacion($youtube, 'required', 'Youtube');
        $error .= $this->mantenimiento->validacion($instagram, 'required', 'Instagram');
        $error .= $this->mantenimiento->validacion($whatsapp, 'required', 'Whatsapp');
        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }
        
        $tmp_facebook['valor'] = $facebook;
        $this->m_configuracion->actualizar($tmp_facebook, array('campo'=>'facebook'));

        $tmp_youtube['valor'] = $youtube;
        $this->m_configuracion->actualizar($tmp_youtube, array('campo'=>'youtube'));

        $tmp_instagram['valor'] = $instagram;
        $this->m_configuracion->actualizar($tmp_instagram, array('campo'=>'instagram'));

        $tmp_whatsapp['valor'] = $whatsapp;
        $this->m_configuracion->actualizar($tmp_whatsapp, array('campo'=>'whatsapp'));
                   
        echo $this->alerta->swal_success('LOS DATOS SE ACTUALIZARON CORRECTAMENTE...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/configuracion/panel','2000'); exit;
    }


    public function accion_datos_contacto() {
        $direccion = $this->input->post('direccion');
        $telefono = $this->input->post('telefono');
        $correo = $this->input->post('correo');
        $horario = $this->input->post('horario');
        $googlemap = $this->input->post('googlemap');
        
        $error = '';
        $error .= $this->mantenimiento->validacion($telefono, 'required', 'Telefono');
        $error .= $this->mantenimiento->validacion($correo, 'required', 'Correo');
        // $error .= $this->mantenimiento->validacion($direccion, 'required', 'Direccion');
        // $error .= $this->mantenimiento->validacion($googlemap, 'required', 'Mapa de google');
        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }
        
        $tmp_direccion['valor'] = $direccion;
        $this->m_configuracion->actualizar($tmp_direccion, array('campo'=>'direccion'));

        $tmp_telefono['valor'] = $telefono;
        $this->m_configuracion->actualizar($tmp_telefono, array('campo'=>'telefono'));

        // $tmp_correo['valor'] = $correo;
        // $this->m_configuracion->actualizar($tmp_correo, array('campo'=>'correo'));

        // $tmp_horario['valor'] = $horario;
        // $this->m_configuracion->actualizar($tmp_horario, array('campo'=>'horario'));

        // $tmp_googlemap['valor'] = $googlemap;
        // $this->m_configuracion->actualizar($tmp_googlemap, array('campo'=>'googlemap'));
                   
        echo $this->alerta->swal_success('LOS DATOS SE ACTUALIZARON CORRECTAMENTE...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/configuracion/panel','2000'); exit;
    }


    public function accion_otros_datos() {
        $video_inclusiva = $this->input->post('video_inclusiva');
        
        $error = '';
        $error .= $this->mantenimiento->validacion($video_inclusiva, 'required', 'Video para interna de Inclusiva');
        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }
        
        $tmp_video_inclusiva['valor'] = $video_inclusiva;
        $this->m_configuracion->actualizar($tmp_video_inclusiva, array('campo'=>'video_inclusiva'));
                   
        echo $this->alerta->swal_success('LOS DATOS SE ACTUALIZARON CORRECTAMENTE...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/configuracion/panel','2000'); exit;
    }


    public function accion_imagenes_chamba() {
        $imagen_chamba_1 = $this->archivo->archivo_1('imagen_chamba_1', 'single');
        $imagen_chamba_2 = $this->archivo->archivo_1('imagen_chamba_2', 'single');
        $imagen_chamba_3 = $this->archivo->archivo_1('imagen_chamba_3', 'single');


        if ($imagen_chamba_1 !== FALSE){
            $tmp1 = $this->m_configuracion->mostrar(array('c.campo' => 'imagen_chamba_1'));
            $mark = array('marca' => '', 'tipo' => 'string');
            $n_imagen = $this->archivo->guardar_imagen($imagen_chamba_1, 'assets/images/extras/', $mark, 1600, $this->items['proyecto']);
            $this->archivo->eliminar_imagen($tmp1['valor'], 'assets/images/extras/');
            $data['valor'] = $n_imagen;
            $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'imagen_chamba_1'));
        }

        if ($imagen_chamba_2 !== FALSE){
            $tmp1 = $this->m_configuracion->mostrar(array('c.campo' => 'imagen_chamba_2'));
            $mark = array('marca' => '', 'tipo' => 'string');
            $n_imagen = $this->archivo->guardar_imagen($imagen_chamba_2, 'assets/images/extras/', $mark, 1600, $this->items['proyecto']);
            $this->archivo->eliminar_imagen($tmp1['valor'], 'assets/images/extras/');
            $data['valor'] = $n_imagen;
            $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'imagen_chamba_2'));
        }

        if ($imagen_chamba_3 !== FALSE){
            $tmp1 = $this->m_configuracion->mostrar(array('c.campo' => 'imagen_chamba_3'));
            $mark = array('marca' => '', 'tipo' => 'string');
            $n_imagen = $this->archivo->guardar_imagen($imagen_chamba_3, 'assets/images/extras/', $mark, 1600, $this->items['proyecto']);
            $this->archivo->eliminar_imagen($tmp1['valor'], 'assets/images/extras/');
            $data['valor'] = $n_imagen;
            $resultSet = $this->m_configuracion->actualizar($data, array('campo'=>'imagen_chamba_3'));
        }

        
        
        
                   
        echo $this->alerta->swal_success('LOS DATOS SE ACTUALIZARON CORRECTAMENTE...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/configuracion/panel','2000'); exit;
    }

}

