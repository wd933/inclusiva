<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Noticia extends CI_Controller {
    public function __construct() {
        parent::__construct();
        /*
         * DECLARACION DE LIBRERIAS, HELPERS Y MODELOS
         */
        $library = array('session_manager', 'archivo', 'orden');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_noticia');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * CONFIGURACION PERSONAL
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Listado de Noticias';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_noticia->mostrar_activos(FALSE, FALSE, ["n.fecha_registro"=>"asc"]);        
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {
                $accion = $this->mantenimiento->accion($items['idnoticia'], 'editar2|eliminar', 'noticia', $items['oculto']);
                    $data['lista'][] = array(
                        'id' => $items['idnoticia'],
                        'numero' => $i,
                        'imagen' => $items['imagen'],
                        'titulo' => $items['titulo'],
                        'accion' => $accion,
                    );
                    $i++;
                }
            }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Listado de Noticias';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_noticia", $data);
    }

    public function agregar(){
        $login = $this->session_manager->datos_usuario_logueado();
        $data = array();

        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view('admin/view/modal_noticia', $data, TRUE);
        $datos['titulo'] = "Agregar Noticia";
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function editar($id = '') {
        $login = $this->session_manager->datos_usuario_logueado();
        if ($id == '') {
            echo $this->url_comp->direccionar(base_url() . 'admin/noticia/listar', TRUE);exit;
        }
        
        $where = array('n.idnoticia' => $id, 'n.oculto' => 0);
        $temp = $this->m_noticia->mostrar($where);
        if (!empty($temp)) {
            $data['id'] = $temp['idnoticia'];
            $data['titulo'] = $temp['titulo'];
            $data['url'] = $temp['url'];
            $data['subtitular'] = $temp['subtitular'];
            $data['imagen'] = $temp['imagen'];
        } else {
            echo $this->url_comp->direccionar(base_url() . 'admin/noticia/listar', TRUE);exit;
        }
        /* ------------------------------------------------------------ */
        $datos['titulo'] = "Editar Noticia";
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view("admin/view/modal_noticia", $data, TRUE);
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function accion() {
        $id = $this->input->post('id');
        $titulo = $this->input->post('titulo');
        $subtitular = $this->input->post('subtitular');
        $url = $this->input->post('url');
        $imagen = $this->archivo->archivo_1('imagen', 'single');
       
        $error = '';
        $error .= $this->mantenimiento->validacion($titulo, 'required', 'Titulo');
        $error .= $this->mantenimiento->validacion($subtitular, 'required', 'Subtitular');
        $error .= $this->mantenimiento->validacion($url, 'required', 'Url');        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }

        if ($id == '') {  
            if ($imagen !== FALSE){
                $mark = array('marca' => '', 'tipo' => 'string');
                $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/noticias', $mark, 1600, $this->items['proyecto']);
            }else{
                echo $this->alerta->swal_error('Debe Elegir una Imagen...', TRUE); exit;
            }

            $datos['titulo'] = $titulo;
            $datos['url'] = $url;
            $datos['subtitular'] = $subtitular;
            $datos['imagen'] = $newImagen;
            $datos['fecha_registro'] = date('Y-m-d H:i:s');
            $datos['fecha_modificacion'] = date('Y-m-d H:i:s');
            
            $result = $this->m_noticia->insertar($datos);
            if($result){
                echo $this->alerta->swal_success('Se registro correctamente...');
                echo $this->url_comp->actualizar_tiempo('1500'); exit;
            }else{
                echo $this->alerta->swal_error('Hubo problemas...', TRUE); exit;
            }            
        }else { //EDITAR
            $where = array('n.idnoticia' => $id, 'n.oculto' => 0);
            $tmpDatos = $this->m_noticia->mostrar($where); 
            if (!empty($tmpDatos)) {
                if ($imagen !== FALSE){
                    $mark = array('marca' => '', 'tipo' => 'string');
                    $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/noticias', $mark, 1600, $this->items['proyecto']);
                    $this->archivo->eliminar_imagen($tmpDatos['imagen'], 'assets/images/noticias');
                }else{
                    $newImagen = $tmpDatos['imagen'];
                }
                
                $datos['titulo'] = $titulo;
                $datos['url'] = $url;
                $datos['subtitular'] = $subtitular;
                $datos['imagen'] = $newImagen;
                $datos['fecha_modificacion'] = date('Y-m-d H:i:s');

                $resultSet = $this->m_noticia->actualizar($datos, 'idnoticia', $tmpDatos['idnoticia']);
                if ($resultSet === TRUE) {
                    echo $this->alerta->swal_success('Se ha editado correctamente...');
                    echo $this->url_comp->direccionar_tiempo(base_url().'admin/noticia/listar','1500');exit;
                }
                
            } else {
                echo $this->url_comp->direccionar(base_url() . 'admin/noticia/listar', TRUE);exit;
            }
        }

    }


    public function accion_eliminar($id = '') {
        if ($id == '') {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/noticia/listar', TRUE); exit;
        }
        $where = array('n.idnoticia' => $id, 'n.oculto' => 0);
        $resultSet = $this->m_noticia->exists($where);
        if ($resultSet === FALSE) {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/noticia/listar', TRUE); exit;
        }
        $imagen = $this->m_noticia->mostrar($where);
        $this->archivo->eliminar_imagen($imagen['imagen'], 'assets/images/noticias');
        $this->m_noticia->eliminar($id);
        echo $this->url_comp->actualizar_tiempo('1200'); exit;
    }


}

