<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Boletin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        /*
         * DECLARACION DE LIBRERIAS, HELPERS Y MODELOS
         */
        $library = array('session_manager', 'archivo', 'orden', 'documento');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_boletin');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * CONFIGURACION PERSONAL
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Listado de Boletines';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_boletin->mostrar_activos(FALSE, FALSE, ["b.fecha_registro"=>"asc"]);        
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {
                $accion = $this->mantenimiento->accion($items['idboletin'], 'editar2|eliminar', 'boletin', $items['oculto']);
                    $data['lista'][] = array(
                        'id' => $items['idboletin'],
                        'numero' => $i,
                        'imagen' => $items['imagen'],
                        'pdf' => $items['pdf'],
                        'accion' => $accion,
                    );
                    $i++;
                }
            }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Listado de Boletines';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_boletin", $data);
    }

    public function agregar(){
        $login = $this->session_manager->datos_usuario_logueado();
        $data = array();

        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view('admin/view/modal_boletin', $data, TRUE);
        $datos['titulo'] = "Agregar Boletin";
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function editar($id = '') {
        $login = $this->session_manager->datos_usuario_logueado();
        if ($id == '') {
            echo $this->url_comp->direccionar(base_url() . 'admin/boletin/listar', TRUE);exit;
        }
        
        $where = array('b.idboletin' => $id, 'b.oculto' => 0);
        $temp = $this->m_boletin->mostrar($where);
        if (!empty($temp)) {
            $data['id'] = $temp['idboletin'];
            $data['imagen'] = $temp['imagen'];
            $data['archivo'] = $temp['pdf'];
        } else {
            echo $this->url_comp->direccionar(base_url() . 'admin/boletin/listar', TRUE);exit;
        }
        /* ------------------------------------------------------------ */
        $datos['titulo'] = "Editar Boletin";
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view("admin/view/modal_boletin", $data, TRUE);
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function accion() {
        $id = $this->input->post('id');
        $imagen = $this->archivo->archivo_1('imagen', 'single');
        $archivo = $this->documento->tipo_documento('archivo', 'single');

        if ($id == '') {  
            if ($imagen !== FALSE){
                $mark = array('marca' => '', 'tipo' => 'string');
                $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/boletines', $mark, 1600, $this->items['proyecto']);
            }else{
                echo $this->alerta->swal_error('Debe Elegir una Imagen...', TRUE); exit;
            }

            if ($archivo !== FALSE){
                $mark = array('marca' => '', 'tipo' => 'string');
                $newArchivo = $this->documento->save_documento($archivo, 'assets/pdf', 25);
            }else{
                echo $this->alerta->swal_error('Debe seleccionar un archivo o no es un archivo valido', TRUE);
                exit;
            }

            $datos['imagen'] = $newImagen;
            $datos['pdf'] = $newArchivo;
            $datos['fecha_registro'] = date('Y-m-d H:i:s');
            $datos['fecha_modificacion'] = date('Y-m-d H:i:s');
            
            $result = $this->m_boletin->insertar($datos);
            if($result){
                echo $this->alerta->swal_success('Se registro correctamente...');
                echo $this->url_comp->actualizar_tiempo('1500'); exit;
            }else{
                echo $this->alerta->swal_error('Hubo problemas...', TRUE); exit;
            }            
        }else { //EDITAR
            $where = array('b.idboletin' => $id, 'b.oculto' => 0);
            $tmpDatos = $this->m_boletin->mostrar($where); 
            if (!empty($tmpDatos)) {
                if ($imagen !== FALSE){
                    $mark = array('marca' => '', 'tipo' => 'string');
                    $newImagen = $this->archivo->guardar_imagen($imagen, 'assets/images/boletines', $mark, 1600, $this->items['proyecto']);
                    $this->archivo->eliminar_imagen($tmpDatos['imagen'], 'assets/images/boletines');
                }else{
                    $newImagen = $tmpDatos['imagen'];
                }

                if ($archivo !== FALSE){
                    $mark = array('marca' => '', 'tipo' => 'string');
                    $newArchivo = $this->documento->save_documento($archivo, 'assets/pdf', 25);
                    $this->archivo->eliminar_imagen($tmpDatos['pdf'], 'assets/pdf');
                }else{
                    $newArchivo = $tmpDatos['archivo'];
                }

                $datos['imagen'] = $newImagen;
                $datos['pdf'] = $newArchivo;
                $datos['fecha_modificacion'] = date('Y-m-d H:i:s');

                $resultSet = $this->m_boletin->actualizar($datos, 'idboletin', $tmpDatos['idboletin']);
                if ($resultSet === TRUE) {
                    echo $this->alerta->swal_success('Se ha editado correctamente...');
                    echo $this->url_comp->direccionar_tiempo(base_url().'admin/boletin/listar','1500');exit;
                }
                
            } else {
                echo $this->url_comp->direccionar(base_url() . 'admin/boletin/listar', TRUE);exit;
            }
        }

    }


    public function accion_eliminar($id = '') {
        if ($id == '') {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/boletin/listar', TRUE); exit;
        }
        $where = array('b.idboletin' => $id, 'b.oculto' => 0);
        $resultSet = $this->m_boletin->exists($where);
        if ($resultSet === FALSE) {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/boletin/listar', TRUE); exit;
        }
        $imagen = $this->m_boletin->mostrar($where);
        $this->archivo->eliminar_imagen($imagen['imagen'], 'assets/images/boletines');
        $this->archivo->eliminar_imagen($imagen['pdf'], 'assets/pdf');
        $this->m_boletin->eliminar($id);
        echo $this->url_comp->actualizar_tiempo('1200'); exit;
    }


}

