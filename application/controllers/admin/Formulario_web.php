<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Formulario_web extends CI_Controller {

    private $_process;
    private $_result;

    public function __construct() {
        parent::__construct();
        
        $library = array('session_manager', 'archivo');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_formulario_web');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * CONFIGURACION PERSONAL
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Listado de Formulario Web';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_formulario_web->mostrar_activos(FALSE, FALSE, array("f.fecha_registro" => "desc"));        
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {
                $accion = $this->mantenimiento->accion($items['idformulario'], 'ver2', 'formulario_web', $items['oculto']);
                    $data['lista'][] = array(
                        'id' => $items['idformulario'],
                        'numero' => $i,
                        'nombre' => $items['nombre'],
                        'email' => $items['email'],
                        'telefono' => $items['telefono'],
                        'f_registro' => date("d-m-Y g:s a", strtotime($items['fecha_registro'])),
                        'accion' => $accion,
                    );
                    $i++;
                }
            }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Solicitudes de los Formularios web';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_formulario_web", $data);
    }

    
    public function observar($id = '') {
        $login = $this->session_manager->datos_usuario_logueado();
        if ($id == '') {
            echo $this->url_comp->direccionar(base_url() . 'admin/formulario_web/listar', TRUE);
            EXIT;
        }
        /* ------------------------------------------------------------ */
        $where = array('f.idformulario' => $id, 'f.oculto' => 0);
        $tmp = $this->m_formulario_web->mostrar($where);
        if (!empty($tmp)) {
            $data['id'] = $tmp['idformulario'];
            $data['nombre'] = $tmp['nombre'];
            $data['telefono'] = $tmp['telefono'];
            $data['email'] = $tmp['email'];
            $data['fecha'] = date("d-m-Y g:s a", strtotime($tmp['fecha_registro']));
            $data['mensaje'] = $tmp['mensaje'];
        } else {
            echo $this->url_comp->direccionar(base_url() . 'admin/formulario_web/listar', TRUE);
            EXIT;
        }
        /* ------------------------------------------------------------ */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view("admin/view/modal_datos_formulario", $data, TRUE);
        //var_dump($contenido); exit;
        $datos['titulo'] = "Datos de la Solicitud";
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }

}

