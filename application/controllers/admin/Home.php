<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        /*
         * Configuración para librerias, helpers y modelos
         */
        $library = array('session_manager');
        $helper = array();
        $model = array('m_configuracion', 'm_usuario');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);

        /* Configuración personalizada */

        $this->items['baseUrl'] = base_url();
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));
        $this->items['time'] = time();

        //var_dump(urlencode(('1523')));

        $array = array(
                'id' => '1',
                'nombre' => 'julio',
                'telefono' => '957635297'
            );

        //var_dump(json_encode($array));
    }

    public function index() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] .' | Inicio';

        
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("home", $data);
    }

    public function panel() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Configuracion';
        $data['tipo'] = 'agregar';
        $data['titulo'] = 'Panel de Configuracion';

        /* ------------------------------------------------------------------ */
        $data['garantia'] = $this->m_configuracion->mostrar(array('c.campo'=>'home_texto_garantia'));
        $data['infraestructura'] = $this->m_configuracion->mostrar(array('c.campo'=>'home_texto_infraestructura'));
        $data['calidad_servicio'] = $this->m_configuracion->mostrar(array('c.campo'=>'home_texto_calidad_servicio'));
        $data['especialidades'] = $this->m_configuracion->mostrar(array('c.campo'=>'home_texto_especialidades'));
       
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("form_home", $data);
    }


    public function accion_panel() {
        $garantia = $this->input->post('garantia');
        $infraestructura = $this->input->post('infraestructura');
        $calidad_servicio = $this->input->post('calidad_servicio');
        $especialidades = $this->input->post('especialidades');
        //var_dump($favicon); exit;
        $error = '';
        $error .= $this->mantenimiento->validacion($garantia, 'required', 'Garantía y alta estética');
        $error .= $this->mantenimiento->validacion($infraestructura, 'required', 'infraestructura');
        $error .= $this->mantenimiento->validacion($calidad_servicio, 'required', 'Calidad de servicio');
        $error .= $this->mantenimiento->validacion($especialidades, 'required', 'Texto de especialidades');

        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE);
            EXIT;
        }

       
        $tmpGarantia['valor'] = $garantia;
        $this->m_configuracion->actualizar($tmpGarantia, array('campo'=>'home_texto_garantia'));

        $tmpInfraestrucura['valor'] = $infraestructura;
        $this->m_configuracion->actualizar($tmpInfraestrucura, array('campo'=>'home_texto_infraestructura'));
       
        $tmpCalidadServicio['valor'] = $calidad_servicio;
        $this->m_configuracion->actualizar($tmpCalidadServicio, array('campo'=>'home_texto_calidad_servicio'));
       
        $tmpEspecialidades['valor'] = $especialidades;
        $this->m_configuracion->actualizar($tmpEspecialidades, array('campo'=>'home_texto_especialidades'));
                            
        echo $this->alerta->swal_success('Se ha actualizaron los datos correctamente...');
        echo $this->url_comp->direccionar_tiempo(base_url().'admin/home/panel','2000');
        EXIT;

    }





    public function login() {
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Login';
        $login = $this->session_manager->verificar_logueo();

        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $this->template->admin_login("login", $data);

    }


    public function recaptcha(){
        $captcha = $this->generar_captcha->captcha();
        echo $captcha['image'];
    }

}

