<?php

@session_cache_limiter('private, must-revalidate');
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", FALSE);
@header("Pragma: no-cache");

class Carrusel_video extends CI_Controller {
    public function __construct() {
        parent::__construct();
        /*
         * DECLARACION DE LIBRERIAS, HELPERS Y MODELOS
         */
        $library = array('session_manager', 'archivo', 'orden');
        $helper = array('base64_url');
        $model = array('m_usuario', 'm_video');
        $this->load->library($library);
        $this->load->helper($helper);
        $this->load->model($model);
        /*
         * CONFIGURACION PERSONAL
         */
        $this->_session = $this->session_manager->datos_usuario('user_data');
        $proyecto = $this->m_configuracion->mostrar(array('c.campo' => 'proyecto_nombre'));
        $this->items['proyecto'] = $proyecto['valor'];
        $this->items['baseUrl'] = base_url();
        $favicon = $this->m_configuracion->mostrar(array('c.campo'=>'favicon'));
        $this->items['favicon_logo'] = $favicon['valor'];
        $this->items['logo'] = $this->m_configuracion->mostrar(array('c.campo'=>'logo'));   
        $this->items['time'] = time();
    }

    public function listar() {
        $login = $this->session_manager->datos_usuario_logueado();
        $data['titulo_pagina'] = $this->items['proyecto'] . ' | Listado de Carrusel Video';
        /* -------------------------------------------------------------------- */
        
        $lista = $this->m_video->mostrar_activos(FALSE, FALSE, ["v.posicion"=>"asc"]);
        if (!empty($lista)) {
            $i = 1;
            foreach ($lista AS $items) {
                $accion = $this->mantenimiento->accion($items['idvideo'], 'subir|bajar|eliminar', 'carrusel_video', $items['oculto']);
                    $data['lista'][] = array(
                        'id' => $items['idvideo'],
                        'numero' => $i,
                        'url' => $items['url'],
                        'codigo' => $this->archivo->youtube($items['url']),
                        'orden' => $items['posicion'],
                        'accion' => $accion,
                    );
                    $i++;
                }
            }

        /* ------------------------------------------------------------------ */
        $data['titulo'] = 'Listado de Carrusel Video';
        /* Impresión de páginas */
        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $this->template->admin("listar_carrusel_video", $data);
    }

    public function agregar(){
        $login = $this->session_manager->datos_usuario_logueado();
        $data = array();

        $data = array_merge($data, $this->items);
        $data = array_merge($data, $login);
        $contenido = $this->smarty_tpl->view('admin/view/modal_carrusel_video', $data, TRUE);
        $datos['titulo'] = "Agregar Carrusel Video";
        $datos['contenido'] = $contenido;
        echo json_encode($datos);
    }


    public function accion() {
        $id = $this->input->post('id');
        $url = $this->input->post('url');        
        
        $error = '';
        $error .= $this->mantenimiento->validacion($url, 'required', 'Url');        
        if ($error != '') {
            echo $this->alerta->swal_error($error, TRUE); exit;
        }

        if ($id == '') {
            $datos['url'] = $url;            
            $result = $this->m_video->insertar_posicion($datos);
            if($result){
                echo $this->alerta->swal_success('Se registro correctamente...');
                echo $this->url_comp->actualizar_tiempo('1500'); exit;
            }else{
                echo $this->alerta->swal_error('Hubo problemas...', TRUE); exit;
            }
            
        }
    }


    public function subir_posicion() {
        $id = $this->input->post('id');
        $resultSet = $this->m_video->exists(array('v.idvideo' => $id, 'v.oculto' => 0));
        if (!empty($resultSet)) {
            $imagen = $this->m_video->mostrar(array('v.idvideo' => $id, 'v.oculto' => 0));
            $lista_imagen = $this->m_video->mostrar_todo();
            $data = array();
            foreach ($lista_imagen as $items) {
                $data[] = (int) $items['posicion'];
            }
            $result = $this->orden->subir($data, $imagen['posicion']);
            $this->m_video->ordenar_posicion($result);
            echo $this->url_comp->actualizar();
            EXIT;
        } else {
            echo $this->alerta->mensaje_error('Hubo problemas', TRUE);
            EXIT;
        }
    }

    public function bajar_posicion() {
        $id = $this->input->post('id');
        $resultSet = $this->m_video->exists(array('v.idvideo' => $id, 'v.oculto' => 0));
        if (!empty($resultSet)) {
            $imagen = $this->m_video->mostrar(array('v.idvideo' => $id, 'v.oculto' => 0));
            $lista_imagen = $this->m_video->mostrar_todo();
            $data = array();
            foreach ($lista_imagen as $items) {
                $data[] = (int) $items['posicion'];
            }
            $result = $this->orden->bajar($data, $imagen['posicion']);
            $this->m_video->ordenar_posicion($result);
            echo $this->url_comp->actualizar();
            EXIT;
        } else {
            echo $this->alerta->mensaje_error('Hubo problemas', TRUE);
            EXIT;
        }
    }


    public function accion_eliminar($id = '') {
        if ($id == '') {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/carrusel_video/listar', TRUE); exit;
        }
        $where = array('v.idvideo' => $id, 'v.oculto' => 0);
        $resultSet = $this->m_video->exists($where);
        if ($resultSet === FALSE) {
            echo $this->url_comp->direccionar(baseUrl() . 'admin/carrusel_video/listar', TRUE); exit;
        }
        $this->m_video->eliminar($id);
        $list = $this->m_video->mostrar_todo();
        $result = array();
        for ($i = 0; $i < count($list); $i++) {
            $result[] = $i + 1;
        }
        $this->m_video->ordenar_posicion($result);
        echo $this->url_comp->actualizar_tiempo('1200'); exit;
    }


}

