<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_carrusel_chamba extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('carrusel_chamba');
        parent::setAlias('cc');
        parent::setTabla_id('idcarruselchamba');
    }

    public function get_query() {
        $this->CI->db->select("cc.*");
        $this->CI->db->from($this->tabla . " cc");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'carrusel_chamba.posicion AS posicion, '
                        . 'carrusel_chamba.idcarruselchamba AS s_id')
                ->from('carrusel_chamba')
                ->order_by('carrusel_chamba.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idcarruselchamba', $items->s_id)->update('carrusel_chamba', $data);
            $i++;
        }
    }

}
