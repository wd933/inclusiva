<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_suscriptor extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('suscriptor');
        parent::setAlias('s');
        parent::setTabla_id('idsuscriptor');
    }

    public function get_query() {
        $this->CI->db->select("s.*");
        $this->CI->db->from($this->tabla . " s");
    }


}
