<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_carrusel_superior extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('carrusel_superior');
        parent::setAlias('cs');
        parent::setTabla_id('idcarruselsuperior');
    }

    public function get_query() {
        $this->CI->db->select("cs.*");
        $this->CI->db->from($this->tabla . " cs");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'carrusel_superior.posicion AS posicion, '
                        . 'carrusel_superior.idcarruselsuperior AS s_id')
                ->from('carrusel_superior')
                ->order_by('carrusel_superior.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idcarruselsuperior', $items->s_id)->update('carrusel_superior', $data);
            $i++;
        }
    }

}
