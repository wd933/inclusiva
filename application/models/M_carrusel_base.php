<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_carrusel_base extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('carrusel_base');
        parent::setAlias('cb');
        parent::setTabla_id('idcarruselbase');
    }

    public function get_query() {
        $this->CI->db->select("cb.*");
        $this->CI->db->from($this->tabla . " cb");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'carrusel_base.posicion AS posicion, '
                        . 'carrusel_base.idcarruselbase AS s_id')
                ->from('carrusel_base')
                ->order_by('carrusel_base.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idcarruselbase', $items->s_id)->update('carrusel_base', $data);
            $i++;
        }
    }

}
