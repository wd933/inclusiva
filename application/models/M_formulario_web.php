<?php

require_once APPPATH . 'libraries/Modelo_DB.php';

class M_formulario_web extends Modelo_DB {

    public function __construct() {
        parent::__construct();
        parent::setTabla('formulario_web');
        parent::setAlias('f');
        parent::setTabla_id('idformulario');
    }

    public function get_query() {
        $this->CI->db->select("f.*");
        $this->CI->db->from($this->tabla . " f");
    }

    public function select_rango($fi, $ff){
        $sql = "SELECT fw.*
            FROM formulario_web fw
            WHERE fw.oculto = 0 AND DATE(fw.fecha_registro) BETWEEN '".$fi."' AND '".$ff."'
            ORDER BY fw.idformulario DESC";

        $resultSet=  $this->CI->db->query($sql)->result_array();
        if (count($resultSet) > 0) {
            return $resultSet;
        } else {
            return false;
        }

        return $data;
    }

}