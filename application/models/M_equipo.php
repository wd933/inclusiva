<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_equipo extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('equipo');
        parent::setAlias('e');
        parent::setTabla_id('idequipo');
    }

    public function get_query() {
        $this->CI->db->select("e.*");
        $this->CI->db->from($this->tabla . " e");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'equipo.posicion AS posicion, '
                        . 'equipo.idequipo AS s_id')
                ->from('equipo')
                ->order_by('equipo.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idequipo', $items->s_id)->update('equipo', $data);
            $i++;
        }
    }

}
