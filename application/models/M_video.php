<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_video extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('video');
        parent::setAlias('v');
        parent::setTabla_id('idvideo');
    }

    public function get_query() {
        $this->CI->db->select("v.*");
        $this->CI->db->from($this->tabla . " v");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'video.posicion AS posicion, '
                        . 'video.idvideo AS s_id')
                ->from('video')
                ->order_by('video.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idvideo', $items->s_id)->update('video', $data);
            $i++;
        }
    }

}
