<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_carrusel_cliente extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('carrusel_cliente');
        parent::setAlias('cc');
        parent::setTabla_id('idcarruselcliente');
    }

    public function get_query() {
        $this->CI->db->select("cc.*");
        $this->CI->db->from($this->tabla . " cc");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'carrusel_cliente.posicion AS posicion, '
                        . 'carrusel_cliente.idcarruselcliente AS s_id')
                ->from('carrusel_cliente')
                ->order_by('carrusel_cliente.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idcarruselcliente', $items->s_id)->update('carrusel_cliente', $data);
            $i++;
        }
    }

}
