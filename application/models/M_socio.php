<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_socio extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('socio');
        parent::setAlias('s');
        parent::setTabla_id('idsocio');
    }

    public function get_query() {
        $this->CI->db->select("s.*");
        $this->CI->db->from($this->tabla . " s");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'socio.posicion AS posicion, '
                        . 'socio.idsocio AS s_id')
                ->from('socio')
                ->order_by('socio.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idsocio', $items->s_id)->update('socio', $data);
            $i++;
        }
    }

}
