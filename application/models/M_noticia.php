<?php

require_once APPPATH . 'libraries/Modelo_DB.php';

class M_noticia extends Modelo_DB {

    public function __construct() {
        parent::__construct();
        parent::setTabla('noticia');
        parent::setAlias('n');
        parent::setTabla_id('idnoticia');
    }

    public function get_query() {
        $this->CI->db->select("n.*");
        $this->CI->db->from($this->tabla . " n");
    }

}
