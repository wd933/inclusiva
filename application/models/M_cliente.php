<?php
require_once APPPATH . 'libraries/Modelo_DB.php';

class M_cliente extends Modelo_DB {
    public function __construct() {
        parent::__construct();
        parent::setTabla('cliente');
        parent::setAlias('c');
        parent::setTabla_id('idcliente');
    }

    public function get_query() {
        $this->CI->db->select("c.*");
        $this->CI->db->from($this->tabla . " c");
    }


    public function ordenar_posicion($order) {
        $images = $this->CI->db->select(''
                        . 'cliente.posicion AS posicion, '
                        . 'cliente.idcliente AS s_id')
                ->from('cliente')
                ->order_by('cliente.posicion', 'asc')
                ->get()
                ->result();
        $i = 0;
        foreach ($images as $items) {
            $data = array(
                'posicion' => $order[$i]
            );
            $this->CI->db->where('idcliente', $items->s_id)->update('cliente', $data);
            $i++;
        }
    }

}
