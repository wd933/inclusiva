<?php

require_once APPPATH . 'libraries/Modelo_DB.php';

class M_boletin extends Modelo_DB {

    public function __construct() {
        parent::__construct();
        parent::setTabla('boletin');
        parent::setAlias('b');
        parent::setTabla_id('idboletin');
    }

    public function get_query() {
        $this->CI->db->select("b.*");
        $this->CI->db->from($this->tabla . " b");
    }

}
