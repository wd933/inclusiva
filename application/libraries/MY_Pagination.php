<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pagination extends CI_Pagination {

	function generate_pagination($url,$cantidad,$total,$segment=5,$sufijo=""){
		parse_str($_SERVER["QUERY_STRING"],$query);
		$config['suffix'] = ($query ? '?'.http_build_query($query) : '');
		$config['first_url'] = $url.($query ? '?'.http_build_query($query) : '');
		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $cantidad;
		$config['num_links'] = 6;
		$config['use_page_numbers'] = false;
		$config['next_link'] = '&rarr;';
		$config['prev_link'] = '&larr;';
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li class="first_page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li class="last_page">';
		$config['last_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#" onclick="return false">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['uri_segment'] = $segment;
		$this->initialize($config);
		return $this->create_links();
	}
}