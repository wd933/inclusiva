<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once(APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php');
 
class Php_mailer extends PHPMailer {
 
    function __construct()
    {
        parent::__construct();
        $this->CharSet = 'UTF-8';
        $this->AltBody = 'Usted debe visualizar este mensaje en un navegador compatible con HTML.';
    }

}