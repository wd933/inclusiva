/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50645
Source Host           : localhost:3306
Source Database       : inclusiva

Target Server Type    : MYSQL
Target Server Version : 50645
File Encoding         : 65001

Date: 2021-10-17 22:42:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acceso
-- ----------------------------
DROP TABLE IF EXISTS `acceso`;
CREATE TABLE `acceso` (
  `idacceso` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) DEFAULT NULL,
  `accion` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idacceso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acceso
-- ----------------------------
INSERT INTO `acceso` VALUES ('1', '::1', '2', null, '2021-08-17 22:31:39', '0');

-- ----------------------------
-- Table structure for bitacora
-- ----------------------------
DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE `bitacora` (
  `idbitacora` int(11) NOT NULL AUTO_INCREMENT,
  `modulo` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `tipo` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idbitacora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bitacora
-- ----------------------------

-- ----------------------------
-- Table structure for boletin
-- ----------------------------
DROP TABLE IF EXISTS `boletin`;
CREATE TABLE `boletin` (
  `idboletin` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idboletin`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of boletin
-- ----------------------------
INSERT INTO `boletin` VALUES ('1', 'inclusiva-boletin-inclusiva-1634525141.png', 'ejemplo_esp-1634525409.pdf', '2021-10-17 21:45:41', '2021-10-17 21:50:09', '0');
INSERT INTO `boletin` VALUES ('3', 'inclusiva-boletin-inclusiva-1634525581.png', 'ejemplo_esp-1634525581.pdf', '2021-10-17 21:53:01', '2021-10-17 21:53:01', '0');
INSERT INTO `boletin` VALUES ('4', 'inclusiva-boletin-inclusiva-1634525596.png', 'ejemplo_esp-1634525596.pdf', '2021-10-17 21:53:16', '2021-10-17 21:53:16', '0');
INSERT INTO `boletin` VALUES ('5', 'inclusiva-boletin-inclusiva-1634525611.png', 'ejemplo_esp-1634525611.pdf', '2021-10-17 21:53:31', '2021-10-17 21:53:31', '0');

-- ----------------------------
-- Table structure for carrusel_base
-- ----------------------------
DROP TABLE IF EXISTS `carrusel_base`;
CREATE TABLE `carrusel_base` (
  `idcarruselbase` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(255) DEFAULT NULL,
  `posicion` int(1) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcarruselbase`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carrusel_base
-- ----------------------------
INSERT INTO `carrusel_base` VALUES ('1', 'inclusiva-bloqueblanco-base-1631600172.png', '1', '0');

-- ----------------------------
-- Table structure for carrusel_chamba
-- ----------------------------
DROP TABLE IF EXISTS `carrusel_chamba`;
CREATE TABLE `carrusel_chamba` (
  `idcarruselchamba` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(255) DEFAULT NULL,
  `posicion` int(1) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcarruselchamba`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carrusel_chamba
-- ----------------------------
INSERT INTO `carrusel_chamba` VALUES ('1', 'inclusiva-bannerazul-min-1631594575.png', '1', '0');

-- ----------------------------
-- Table structure for carrusel_cliente
-- ----------------------------
DROP TABLE IF EXISTS `carrusel_cliente`;
CREATE TABLE `carrusel_cliente` (
  `idcarruselcliente` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcarruselcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carrusel_cliente
-- ----------------------------
INSERT INTO `carrusel_cliente` VALUES ('1', 'inclusiva-bannerclientes-1631585809.png', '1', '0');

-- ----------------------------
-- Table structure for carrusel_superior
-- ----------------------------
DROP TABLE IF EXISTS `carrusel_superior`;
CREATE TABLE `carrusel_superior` (
  `idcarruselsuperior` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcarruselsuperior`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carrusel_superior
-- ----------------------------
INSERT INTO `carrusel_superior` VALUES ('1', 'inclusiva-portada-home-1631591034.png', '1', '0');

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES ('1', 'inclusiva-copia-de-1280px-logo_grupo_bimbo-1629960457.png', '4', '2021-08-26 01:47:37', '1', '0');
INSERT INTO `cliente` VALUES ('2', 'inclusiva-copia-de-dimexa-1629960524.png', '2', '2021-08-26 01:48:44', '1', '0');
INSERT INTO `cliente` VALUES ('3', 'inclusiva-copia-de-ecosac-1629960531.png', '1', '2021-08-26 01:48:51', '1', '0');
INSERT INTO `cliente` VALUES ('4', 'inclusiva-copia-de-fenix-power-1629960542.jpg', '3', '2021-08-26 01:49:02', '1', '0');
INSERT INTO `cliente` VALUES ('8', 'inclusiva-copia-de-logo-hochschild-1629960761.png', '5', '2021-08-26 01:52:41', '1', '0');
INSERT INTO `cliente` VALUES ('9', 'inclusiva-copia-de-img_5138-1629960807.jpg', '6', '2021-08-26 01:53:27', '1', '0');
INSERT INTO `cliente` VALUES ('10', 'inclusiva-copia-de-inppares-1629960819.jpg', '7', '2021-08-26 01:53:39', '1', '0');
INSERT INTO `cliente` VALUES ('11', 'inclusiva-copia-de-logo-agriex-1629963113.jpg', '8', '2021-08-26 02:31:53', '1', '0');
INSERT INTO `cliente` VALUES ('12', 'inclusiva-copia-de-logo-alicorp-1629963126.jpg', '9', '2021-08-26 02:32:06', '1', '0');
INSERT INTO `cliente` VALUES ('13', 'inclusiva-copia-de-hortifrut-peru-1629963133.png', '10', '2021-08-26 02:32:13', '1', '0');
INSERT INTO `cliente` VALUES ('14', 'inclusiva-copia-de-logo-marka-group-1629963157.png', '11', '2021-08-26 02:32:37', '1', '0');
INSERT INTO `cliente` VALUES ('15', 'inclusiva-copia-de-logo-vipo-1629963226.png', '12', '2021-08-26 02:33:46', '1', '0');
INSERT INTO `cliente` VALUES ('16', 'inclusiva-copia-de-logo-1629963238.jpg', '13', '2021-08-26 02:33:59', '1', '0');
INSERT INTO `cliente` VALUES ('17', 'inclusiva-copia-de-logo-fondoempleo-1629963276.png', '14', '2021-08-26 02:34:36', '1', '0');
INSERT INTO `cliente` VALUES ('18', 'inclusiva-copia-de-logo-grupo-el-rocio-1629963284.jpg', '15', '2021-08-26 02:34:44', '1', '0');
INSERT INTO `cliente` VALUES ('19', 'inclusiva-copia-de-mibanco-1629963293.jpg', '16', '2021-08-26 02:34:53', '1', '0');
INSERT INTO `cliente` VALUES ('20', 'inclusiva-copia-de-midis-1629963304.png', '17', '2021-08-26 02:35:04', '1', '0');
INSERT INTO `cliente` VALUES ('21', 'inclusiva-copia-de-nestle-logo-900-1629963314.jpg', '18', '2021-08-26 02:35:14', '1', '0');
INSERT INTO `cliente` VALUES ('22', 'inclusiva-copia-de-pacasmayo-01-1629963323.jpg', '19', '2021-08-26 02:35:23', '1', '0');
INSERT INTO `cliente` VALUES ('23', 'inclusiva-copia-de-palmas-1629963332.jpg', '20', '2021-08-26 02:35:32', '1', '0');
INSERT INTO `cliente` VALUES ('24', 'inclusiva-copia-de-snv-1629963350.png', '21', '2021-08-26 02:35:50', '1', '0');
INSERT INTO `cliente` VALUES ('25', 'inclusiva-copia-de-sunass-1629963358.png', '22', '2021-08-26 02:35:58', '1', '0');
INSERT INTO `cliente` VALUES ('26', 'inclusiva-copia-de-usaid-1629963372.jpg', '23', '2021-08-26 02:36:12', '1', '0');

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `valor` text COLLATE utf8_spanish_ci,
  `oculto` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Records of configuracion
-- ----------------------------
INSERT INTO `configuracion` VALUES ('1', 'proyecto_nombre', 'INCLUSIVA', '0');
INSERT INTO `configuracion` VALUES ('2', 'logo', 'logo-inclusiva.png', '0');
INSERT INTO `configuracion` VALUES ('3', 'favicon', 'favicon.ico', '0');
INSERT INTO `configuracion` VALUES ('4', 'facebook', 'https://www.facebook.com/InclusivaConsulting', '0');
INSERT INTO `configuracion` VALUES ('5', 'twitter', 'https://twitter.com/InclusivaC', '0');
INSERT INTO `configuracion` VALUES ('6', 'google', '', '0');
INSERT INTO `configuracion` VALUES ('7', 'linkedin', 'https://www.linkedin.com/company/inclusiva', '0');
INSERT INTO `configuracion` VALUES ('8', 'direccion', '', '0');
INSERT INTO `configuracion` VALUES ('9', 'telefono', '', '0');
INSERT INTO `configuracion` VALUES ('10', 'celular', '', '0');
INSERT INTO `configuracion` VALUES ('11', 'correo', 'contacto@inclusiva.com.pe', '0');
INSERT INTO `configuracion` VALUES ('12', 'googlemap', '', '0');
INSERT INTO `configuracion` VALUES ('13', 'mensaje', '', '0');
INSERT INTO `configuracion` VALUES ('14', 'youtube', 'https://www.youtube.com/channel/UCSwVzELrBDz6_RsLAxpI9Pg', '0');
INSERT INTO `configuracion` VALUES ('19', 'whatsapp', '999999999', '0');
INSERT INTO `configuracion` VALUES ('20', 'instagram', 'https://www.instagram.com/inclusivaconsulting/', '0');
INSERT INTO `configuracion` VALUES ('21', 'video_inclusiva', 'https://www.youtube.com/watch?v=RWrkqpR0Hq4', '0');
INSERT INTO `configuracion` VALUES ('22', 'imagen_chamba_1', 'inclusiva-nuestrachamba-1-1631596682.png', '0');
INSERT INTO `configuracion` VALUES ('23', 'imagen_chamba_2', 'inclusiva-nuestrachamba-2-1631596682.png', '0');
INSERT INTO `configuracion` VALUES ('24', 'imagen_chamba_3', 'inclusiva-nuestrachamba-3-1631596682.png', '0');

-- ----------------------------
-- Table structure for control
-- ----------------------------
DROP TABLE IF EXISTS `control`;
CREATE TABLE `control` (
  `idcontrol` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonal` int(11) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `accion` varchar(50) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idcontrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control
-- ----------------------------

-- ----------------------------
-- Table structure for formulario_web
-- ----------------------------
DROP TABLE IF EXISTS `formulario_web`;
CREATE TABLE `formulario_web` (
  `idformulario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mensaje` text,
  `fecha_registro` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idformulario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of formulario_web
-- ----------------------------
INSERT INTO `formulario_web` VALUES ('1', 'prueba', '999999999', 'prueba@gmail.com', 'prueba', '2021-09-27 23:12:34', '::1', '0');

-- ----------------------------
-- Table structure for nivel_usuario
-- ----------------------------
DROP TABLE IF EXISTS `nivel_usuario`;
CREATE TABLE `nivel_usuario` (
  `idnivel` int(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `oculto` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idnivel`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nivel_usuario
-- ----------------------------
INSERT INTO `nivel_usuario` VALUES ('1', 'Sistemas', '0');
INSERT INTO `nivel_usuario` VALUES ('2', 'Super Administrador', '0');
INSERT INTO `nivel_usuario` VALUES ('3', 'Web Master', '0');
INSERT INTO `nivel_usuario` VALUES ('4', 'Gestor de Sedes', '0');
INSERT INTO `nivel_usuario` VALUES ('5', 'Marketing', '0');
INSERT INTO `nivel_usuario` VALUES ('6', 'Bloger', '0');
INSERT INTO `nivel_usuario` VALUES ('7', 'Administradora de Sede', '0');

-- ----------------------------
-- Table structure for noticia
-- ----------------------------
DROP TABLE IF EXISTS `noticia`;
CREATE TABLE `noticia` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitular` text,
  `url` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idnoticia`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of noticia
-- ----------------------------
INSERT INTO `noticia` VALUES ('2', '27-28 de julio: Conferencia VIRTUAL de participación de los empleados de HCI', 'Únase a nosotros en la conferencia virtual Employee Engagement de HCI para aprender cómo crear vías abiertas de comunicación en el lugar de trabajo virtual', 'https://inclusiva.hospedandoperu.com/', 'inclusiva-noticia-2-1631722877.jpg', '2021-09-15 11:21:17', '2021-09-15 11:21:17', '0');
INSERT INTO `noticia` VALUES ('3', '27-28 de julio: Conferencia VIRTUAL de participación de los empleados de HCI', 'Únase a nosotros en la conferencia virtual Employee Engagement de HCI para aprender cómo crear vías abiertas de comunicación en el lugar de trabajo virtual', 'https://inclusiva.hospedandoperu.com/', 'inclusiva-noticia-1-1631723978.jpg', '2021-09-15 11:39:38', '2021-09-15 11:39:38', '0');

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `idslider` int(11) NOT NULL AUTO_INCREMENT,
  `titulo1` varchar(150) DEFAULT NULL,
  `titulo2` varchar(100) DEFAULT NULL,
  `subtitulo` varchar(150) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `oculto` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idslider`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slider
-- ----------------------------

-- ----------------------------
-- Table structure for socio
-- ----------------------------
DROP TABLE IF EXISTS `socio`;
CREATE TABLE `socio` (
  `idsocio` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idsocio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of socio
-- ----------------------------
INSERT INTO `socio` VALUES ('1', 'inclusiva-copia-de-2018-logo-eng-futuralab-1629961643.jpg', '1', '2021-08-26 02:07:23', '1', '0');
INSERT INTO `socio` VALUES ('2', 'inclusiva-copia-de-baoba-1629961656.png', '2', '2021-08-26 02:07:36', '1', '0');
INSERT INTO `socio` VALUES ('3', 'inclusiva-copia-de-campo-base-manual-logotipo-baja-1629961665.png', '3', '2021-08-26 02:07:45', '1', '0');
INSERT INTO `socio` VALUES ('4', 'inclusiva-copia-de-fsq-ds-1629961675.png', '4', '2021-08-26 02:07:55', '1', '0');
INSERT INTO `socio` VALUES ('7', 'inclusiva-copia-de-logo-core-2017-cggris-1629961770.jpg', '5', '2021-08-26 02:09:30', '1', '0');
INSERT INTO `socio` VALUES ('8', 'inclusiva-copia-de-logo-inclusys-alta-resol-1629961780.jpg', '6', '2021-08-26 02:09:40', '1', '0');
INSERT INTO `socio` VALUES ('9', 'inclusiva-copia-de-reboot-1629961793.jpg', '7', '2021-08-26 02:09:53', '1', '0');

-- ----------------------------
-- Table structure for suscriptor
-- ----------------------------
DROP TABLE IF EXISTS `suscriptor`;
CREATE TABLE `suscriptor` (
  `idsuscriptor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `lugar_trabajo` varchar(255) DEFAULT NULL,
  `cargo` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idsuscriptor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of suscriptor
-- ----------------------------
INSERT INTO `suscriptor` VALUES ('1', 'demo', 'demo demo', 'demo@gmail.com', 'indra', '', '2021-09-27 23:32:24', '::1', '0');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `idusuario` int(8) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `conectado` datetime NOT NULL,
  `desconectado` datetime NOT NULL,
  `idnivel` int(8) NOT NULL,
  `idsede` int(8) DEFAULT NULL,
  `oculto` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'admin', 'MTIzNDU2', '2021-10-17 21:23:09', '2016-10-07 17:10:49', '1', '0', '0');

-- ----------------------------
-- Table structure for usuario_datos
-- ----------------------------
DROP TABLE IF EXISTS `usuario_datos`;
CREATE TABLE `usuario_datos` (
  `idusuariodatos` int(8) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(150) NOT NULL,
  `apellidos` varchar(150) DEFAULT NULL,
  `fecha_registro` datetime NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `idusuario` int(1) NOT NULL,
  `oculto` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idusuariodatos`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario_datos
-- ----------------------------
INSERT INTO `usuario_datos` VALUES ('1', 'Inclusiva', 'Admin', '2016-10-07 17:11:13', '', '', '1', '1');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `idvideo` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `posicion` int(1) DEFAULT NULL,
  `oculto` int(1) DEFAULT '0',
  PRIMARY KEY (`idvideo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', 'https://www.youtube.com/watch?v=RWrkqpR0Hq4', '1', '0');
